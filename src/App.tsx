import React from 'react';
import * as PersistTypes from 'redux-persist/es/types';
import 'react-toastify/dist/ReactToastify.css';

import { AuthorizationRoutes } from './routes/AuthorizationRoutes';
import { RootRoutes } from './routes/RootRoutes';
import { selectors } from 'src/store/ducks';
import { useAppSelector } from 'src/store/hooks';

export const App: React.FC<AppProps> = ({ persistor }) => {
  const accessToken = useAppSelector(selectors.user.selectUser);
  return (
    <>
      {accessToken ? (
        <RootRoutes />
      ) : (
        <AuthorizationRoutes persistor={persistor} />
      )}
    </>
  );
};

interface AppProps {
  persistor?: PersistTypes.Persistor;
}
