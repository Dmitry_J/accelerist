import axios, { AxiosInstance } from 'axios';
import { store } from 'src/store/store';

const baseUrl = 'https://accelerist.herokuapp.com';

const httpClient: AxiosInstance = axios.create({
  baseURL: baseUrl,
});

httpClient.interceptors.request.use((config) => {
  const token = store.getState().user.accessToken;
  if (!token) {
    return config;
  }

  if (config.withCredentials === false) {
    return config;
  }

  const headers = {
    Authorization: `Bearer ${token}`,
    ...config.headers,
  };

  return { ...config, headers };
});

httpClient.interceptors.response.use(
  (response) => response,
  (error) => {
    if (error.response) {
      throw error.response.data;
    }

    if (error.data) {
      throw error.data;
    }

    throw error;
  }
);

export const createSourceCancelToken = () => {
  return axios.CancelToken.source();
};

export default httpClient;
