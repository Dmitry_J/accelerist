import httpClient from './apiCLient';
import * as Types from 'src/types/types';

// User

export const signIn = (data: Types.LogInApiTypes) => {
  return httpClient.post('/api/v1/auth/sign_in', {
    ...data,
  });
};

export const signUp = (data: Types.LogInApiTypes) => {
  return httpClient.post('/api/v1/auth/sign_up', {
    ...data,
  });
};

export const sendMailChangePassword = (data: Types.SendMailApiTypes) => {
  return httpClient.post('/api/v1/auth/change_password/send_mail', {
    ...data,
  });
};

// savedList

export const getSavedList = (data: Types.GetSavedList) => {
  if (data.sort) {
    return httpClient.get(
      `/api/v1/saved-list?page=${data.page}&limit=${data.limit}&sort=${data.sort}`
    );
  }
  return httpClient.get(
    `/api/v1/saved-list?page=${data.page}&limit=${data.limit}`
  );
};

// companies

export const getFavoritesCompanies = (data: Types.GetFavoriteCompanies) => {
  return httpClient.get(
    `/api/v1/companies/favorites?page=${data.page}&limit=${data.limit}`
  );
};

export const changeLikeCompany = (data: Types.GetCompany) => {
  if (data.like) {
    return httpClient.get(`/api/v1/companies/${data.id}/dislike`);
  } else {
    return httpClient.get(`/api/v1/companies/${data.id}/like`);
  }
};

export const getOneCompany = (data: Types.GetCompany) => {
  return httpClient.get(`/api/v1/companies/${data.id}`);
};

export const getCompanies = (data: any) => {
  if (data.searchCompany) {
    return httpClient.get(
      `/api/v1/companies?gender=both&limit=15&page=${data.page}&q=${data.searchCompany}`
    );
  } else {
    return httpClient.get(
      `/api/v1/companies?gender=both&limit=15&page=${data.page}`
    );
  }
};

// team

export const getTeam = () => {
  return httpClient.get(`/api/v1/team`);
};
