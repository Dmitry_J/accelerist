import React, { HTMLAttributes } from 'react';
import styled from 'styled-components';

export const EnterFormWrapper = ({
  children,
}: HTMLAttributes<HTMLDivElement>): JSX.Element => {
  return <Root>{children}</Root>;
};

const Root = styled.div`
  width: 100%;
  max-width: 464px;
  margin-bottom: 32px;
  padding: 40px;
  background-color: #ffffff;
  border-radius: 6px;
`;
