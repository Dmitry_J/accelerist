import React from 'react';
import styled from 'styled-components';

import { color } from 'src/styles/color';

export const GrayBlockContainer: React.FC = ({ children }) => {
  return <Root>{children}</Root>;
};

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  background-color: ${color.bg};
  border-radius: 4px;
  padding: 5px;
`;
