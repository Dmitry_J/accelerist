import React, { HTMLAttributes } from 'react';
import styled from 'styled-components';

import bgEnter from 'src/assets/images/bg-enter.png';

export const EnterContainer = ({
  children,
}: HTMLAttributes<HTMLDivElement>): JSX.Element => {
  return <Root>{children}</Root>;
};

const Root = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;
  padding-top: 73px;
  width: 100%;
  min-height: calc(100% - 80px);
  background-image: url(${bgEnter});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: 100% 100%;
`;
