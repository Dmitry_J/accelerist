import React, { HTMLAttributes } from 'react';
import styled from 'styled-components';

export const ContentContainer: React.FC<HTMLAttributes<HTMLDivElement>> = ({
  children,
  ...props
}) => {
  return (
    <Root {...props}>
      <Wrapper>{children}</Wrapper>
    </Root>
  );
};

const Root = styled.div`
  max-width: 1334px;
  margin: auto;
  padding: 0 10px;
`;

const Wrapper = styled.div`
  padding: 32px 0;
  margin-right: 218px;
`;
