import { userReducer } from './user';
import { savedListReducer } from './savedList';
import { companiesReducer } from './companies';

import { userActions } from './user';
import { savedListActions } from './savedList';
import { companiesActions } from './companies';

import { userSelectors } from './user';
import { savedListSelectors } from './savedList';
import { companiesSelectors } from './companies';

import { userThunks } from './user';
import { savedListThunks } from './savedList';
import { companiesThunks } from './companies';

const reducers = {
  user: userReducer,
  savedList: savedListReducer,
  companies: companiesReducer,
};

const actions = {
  user: userActions,
  savedList: savedListActions,
  companies: companiesActions,
};

const selectors = {
  user: userSelectors,
  savedList: savedListSelectors,
  companies: companiesSelectors,
};

const thunks = {
  user: userThunks,
  savedList: savedListThunks,
  companies: companiesThunks,
};

export { reducers, actions, selectors, thunks };
