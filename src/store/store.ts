import { configureStore, combineReducers } from '@reduxjs/toolkit';
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

import { reducers } from './ducks';

const persistConfig = {
  key: 'root',
  storage,
};

const reducer = combineReducers({
  user: reducers.user,
  savedList: reducers.savedList,
  companies: reducers.companies,
});

export const store = configureStore({
  reducer: persistReducer(persistConfig, reducer),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export type RootState = ReturnType<typeof store.getState>;

export type AppDispatch = typeof store.dispatch;
