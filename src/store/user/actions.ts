import { logOutAction, resetError } from './reducers';

export const userActions = {
  logOutAction,
  resetError,
};
