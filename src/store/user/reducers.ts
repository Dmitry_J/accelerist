import { createSlice } from '@reduxjs/toolkit';
import { signInAction, signUpAction } from './thunks';
import { UserLogInType } from './types';
import { RequestTypes } from 'src/types/types';
import storage from 'redux-persist/lib/storage';

const initialState: UserLogInType = {
  accessToken: '',
  statusRequest: null,
  errorMessage: null,
};

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    logOutAction(state) {
      state.accessToken = '';
      state.statusRequest = null;
      storage.removeItem('persist:root');
    },
    resetError(state) {
      state.errorMessage = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(signInAction.pending, (state) => {
      state.statusRequest = RequestTypes.PENDING;
      state.errorMessage = null;
    });
    builder.addCase(signInAction.fulfilled, (state, action) => {
      state.accessToken = action.payload.accessToken;
      state.statusRequest = RequestTypes.FULFILLED;
      state.errorMessage = null;
    });
    builder.addCase(signInAction.rejected, (state, action) => {
      state.errorMessage = action.payload;
      state.statusRequest = RequestTypes.REJECTED;
    });
    builder.addCase(signUpAction.pending, (state) => {
      state.statusRequest = RequestTypes.PENDING;
      state.errorMessage = null;
    });
    builder.addCase(signUpAction.fulfilled, (state, action) => {
      state.accessToken = action.payload.accessToken;
      state.statusRequest = RequestTypes.FULFILLED;
      state.errorMessage = null;
    });
    builder.addCase(signUpAction.rejected, (state, action) => {
      state.errorMessage = action.payload;
      state.statusRequest = RequestTypes.REJECTED;
    });
  },
});

export const { logOutAction, resetError } = userSlice.actions;
export default userSlice.reducer;
