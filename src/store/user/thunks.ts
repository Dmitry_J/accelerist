import { createAsyncThunk } from '@reduxjs/toolkit';
import { signIn, signUp } from 'src/services/apiFunctions';
import { UserLogInType } from './types';
import * as Types from 'src/types/types';
import { AppDispatch } from '../store';

export const signInAction = createAsyncThunk<
  UserLogInType,
  Types.LogInApiTypes,
  {
    dispatch: AppDispatch;
    state: UserLogInType;
  }
>('user/sign_in', async (data, { rejectWithValue }) => {
  try {
    const response = await signIn(data);
    return response.data;
  } catch (err: any | unknown) {
    return rejectWithValue(err.message);
  }
});

export const signUpAction = createAsyncThunk<
  UserLogInType,
  Types.LogInApiTypes,
  {
    dispatch: AppDispatch;
    state: UserLogInType;
  }
>('user/sign_up', async (data, { rejectWithValue }) => {
  try {
    const response = await signUp(data);
    return response.data;
  } catch (err: any | unknown) {
    return rejectWithValue(err.message);
  }
});

export const userThunks = {
  signInAction,
  signUpAction,
};
