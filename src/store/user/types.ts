export interface UserLogInType {
  accessToken: string;
  statusRequest: string | null;
  errorMessage?: string | unknown;
  user?: UserType;
}

interface UserType {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  isAuthorized: boolean;
  imported: boolean;
  teamId: string;
  role: string;
  linkedinLink: null;
  isReceivingNotifications: boolean;
  avatarKey: null;
  loggedInAt: string;
  createdAt: string;
  updatedAt: string;
  deletedAt: null;
}
