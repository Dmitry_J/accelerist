import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const selectUser = createSelector(
  (state: RootState) => state.user,
  (user) => user.accessToken
);

const selectStatus = createSelector(
  (state: RootState) => state.user,
  (user) => user.statusRequest
);

const selectError = createSelector(
  (state: RootState) => state.user,
  (user) => user.errorMessage
);

export const userSelectors = {
  selectUser,
  selectStatus,
  selectError,
};
