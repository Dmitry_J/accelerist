export interface CompaniesInialState {
  items: ItemsTypes[];
  meta: MetaCompaniesTypes;
  statusRequest?: null | string;
  company?: ItemsTypes | null;
}

interface ItemsTypes {
  id: string;
  zoomInfoId: null | string;
  name: string;
  logo: null | string;
  ticker: string;
  parentCompany: null | string;
  phone: string;
  fax: string;
  website: string;
  city: string;
  street: string;
  state: string;
  zipCode: null | string;
  country: string;
  continent: null | string;
  productsBrandDescription: null | string;
  descriptionList: string;
  revenueRange: string;
  employeeRange: string;
  twitterHandle: null | string;
  socialMediaUrls: null | string;
  competitors: null | string;
  subUnitIndustries: null | string;
  primaryIndustry: string[];
  industries: null | string;
  revenue: string;
  employeeCount: number;
  annualContributions: null | string;
  cashContributions: null | string;
  inKindContributions: null | string;
  employeeContributions: null | string;
  parentId: null | string;
  parentName: null | string;
  type: null | string;
  sdgGoals: any[];
  genders: null | string;
  income: null | string;
  age: null | string;
  ethnicity: null | string;
  nonprofit: null | string;
  purchase: null | string;
  affiliation: null | string;
  brands: null | string;
  interests: null | string;
  typesOfInvestment: null | string;
  errorLoadZoomInfo: null | string;
  charitablePartners: any[];
  statusZoomInfo: string;
  loadZoomInfoDate: null | string;
  errorLoadZoomInfoDate: null | string;
  partnershipLink: null | string;
  employeeEngagementOpportunities: boolean;
  similarCompanies: string[];
  favoriteCompanies: FavoriteCompaniesTypes[];
  score: number;
  like: boolean;
  crsFocus: any[];
}

interface FavoriteCompaniesTypes {
  id: string;
  companyId: string;
  userId: string;
}

interface MetaCompaniesTypes {
  totalItems: number;
  itemCount: number;
  itemsPerPage: string;
  totalPages: number;
  currentPage: string;
}
