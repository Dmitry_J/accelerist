import { createAsyncThunk } from '@reduxjs/toolkit';
import {
  getFavoritesCompanies,
  getOneCompany,
  getCompanies,
} from 'src/services/apiFunctions';
import * as Types from 'src/types/types';
import { AppDispatch } from '../store';
import { CompaniesInialState } from './types';

export const asyncGetFavoritesCompanies = createAsyncThunk<
  CompaniesInialState,
  Types.GetFavoriteCompanies,
  {
    dispatch: AppDispatch;
    store: CompaniesInialState;
  }
>('companies/favorites', async (data, { rejectWithValue }) => {
  try {
    const response = await getFavoritesCompanies(data);
    return response.data;
  } catch (err: any | unknown) {
    return rejectWithValue(err.message);
  }
});

export const asyncGetOneCompany = createAsyncThunk(
  'companies/get_company',
  async (data: { id: string }, { rejectWithValue }) => {
    try {
      const response = await getOneCompany(data);
      return response.data;
    } catch (err: any | unknown) {
      return rejectWithValue(err.message);
    }
  }
);

export const asyncGetCompanies = createAsyncThunk(
  'companies/search',
  async (data: any, { rejectWithValue }) => {
    try {
      const response = await getCompanies(data);
      return response.data;
    } catch (err: any | unknown) {
      return rejectWithValue(err.message);
    }
  }
);

export const companiesThunks = {
  asyncGetFavoritesCompanies,
  asyncGetOneCompany,
  asyncGetCompanies,
};
