import { createSlice } from '@reduxjs/toolkit';
import { CompaniesInialState } from './types';
import {
  asyncGetFavoritesCompanies,
  asyncGetOneCompany,
  asyncGetCompanies,
} from './thunks';
import { RequestTypes } from 'src/types/types';

const initialState: CompaniesInialState = {
  items: [],
  meta: {
    totalItems: 0,
    itemCount: 0,
    itemsPerPage: '',
    totalPages: 0,
    currentPage: '',
  },
  statusRequest: null,
  company: null,
};

const companiesSlice = createSlice({
  name: 'companies',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(asyncGetFavoritesCompanies.pending, (state) => {
      state.statusRequest = RequestTypes.PENDING;
      state.items = [];
    });
    builder.addCase(asyncGetFavoritesCompanies.fulfilled, (state, action) => {
      state.statusRequest = RequestTypes.FULFILLED;
      state.items = action.payload.items;
      state.meta = action.payload.meta;
    });
    builder.addCase(asyncGetFavoritesCompanies.rejected, (state) => {
      state.statusRequest = RequestTypes.REJECTED;
      state.items = [];
    });

    builder.addCase(asyncGetOneCompany.pending, (state) => {
      state.statusRequest = RequestTypes.PENDING;
      state.company = null;
    });
    builder.addCase(asyncGetOneCompany.fulfilled, (state, action) => {
      state.statusRequest = RequestTypes.FULFILLED;
      state.company = action.payload;
    });
    builder.addCase(asyncGetOneCompany.rejected, (state) => {
      state.statusRequest = RequestTypes.REJECTED;
      state.company = null;
    });

    builder.addCase(asyncGetCompanies.pending, (state) => {
      state.statusRequest = RequestTypes.PENDING;
      state.items = [];
    });
    builder.addCase(asyncGetCompanies.fulfilled, (state, action) => {
      state.statusRequest = RequestTypes.FULFILLED;
      state.items = action.payload.items;
      state.meta = action.payload.meta;
    });
    builder.addCase(asyncGetCompanies.rejected, (state) => {
      state.statusRequest = RequestTypes.REJECTED;
      state.items = [];
    });
  },
});

// export const { } = companiesSlice.actions;
export default companiesSlice.reducer;
