import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const selectMetaCompanies = createSelector(
  (state: RootState) => state.companies,
  (companies) => companies.meta
);

const selectItemsCompanies = createSelector(
  (state: RootState) => state.companies,
  (companies) => companies.items
);

const selectRequestCompanies = createSelector(
  (state: RootState) => state.companies,
  (companies) => companies.statusRequest
);

const selectCompany = createSelector(
  (state: RootState) => state.companies,
  (companies) => companies.company
);

export const companiesSelectors = {
  selectMetaCompanies,
  selectItemsCompanies,
  selectRequestCompanies,
  selectCompany,
};
