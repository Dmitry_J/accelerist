import { createAsyncThunk } from '@reduxjs/toolkit';
import { getSavedList } from 'src/services/apiFunctions';
import * as Types from 'src/types/types';
import { AppDispatch } from '../store';
import { SavedListInialState, ResponseSavedList } from './types';

export const asyncGetSavedList = createAsyncThunk<
  ResponseSavedList,
  Types.GetSavedList,
  { dispatch: AppDispatch; state: SavedListInialState }
>('savedList/saved_list', async (data, { rejectWithValue }) => {
  try {
    const response = await getSavedList(data);

    const responseData: ResponseSavedList = {
      response: response.data,
      sort: data.sort,
    };
    return responseData;
  } catch (err: any | unknown) {
    return rejectWithValue(err.message);
  }
});

export const savedListThunks = {
  asyncGetSavedList,
};
