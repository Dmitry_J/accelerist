import { createSelector } from '@reduxjs/toolkit';
import { RootState } from '../store';

const selectMetaSavedList = createSelector(
  (state: RootState) => state.savedList,
  (savedList) => savedList.meta
);

const selectItemsSavedList = createSelector(
  (state: RootState) => state.savedList,
  (savedList) => savedList.items
);

const selectRequestSavedList = createSelector(
  (state: RootState) => state.savedList,
  (savedList) => savedList.statusRequest
);

const selectSortSavedList = createSelector(
  (state: RootState) => state.savedList,
  (savedList) => savedList.sort
);

export const savedListSelectors = {
  selectMetaSavedList,
  selectItemsSavedList,
  selectRequestSavedList,
  selectSortSavedList,
};
