export interface SavedListInialState {
  items: ItemsTypes[];
  meta: MetaTypes;
  statusRequest?: string | null;
  sort?: string | null;
}

export interface ResponseSavedList {
  response: SavedListInialState;
  sort: string | null | undefined;
}

interface ItemsTypes {
  id: string;
  name: string;
  filters: any;
  prospectsAvailable: number;
  lastAuthor: any;
  createdAt: string;
  updatedAt: string;
}

interface MetaTypes {
  totalItems: number | null;
  itemCount: number | null;
  itemsPerPage: string | null;
  totalPages: number | null;
  currentPage: string | null;
}
