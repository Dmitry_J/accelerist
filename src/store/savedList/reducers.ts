import { createSlice } from '@reduxjs/toolkit';
import { asyncGetSavedList } from './thunks';
import { SavedListInialState } from './types';
import { RequestTypes } from 'src/types/types';

const initialState: SavedListInialState = {
  items: [],
  meta: {
    totalItems: null,
    itemCount: null,
    itemsPerPage: null,
    totalPages: null,
    currentPage: null,
  },
  statusRequest: null,
  sort: null,
};

const savedListSlice = createSlice({
  name: 'savedList',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(asyncGetSavedList.pending, (state) => {
      state.statusRequest = RequestTypes.PENDING;
    });
    builder.addCase(asyncGetSavedList.fulfilled, (state, action) => {
      state.statusRequest = RequestTypes.FULFILLED;
      state.items = action.payload.response.items;
      state.meta = action.payload.response.meta;
      if (action.payload.sort) {
        state.sort = action.payload.sort;
      } else {
        state.sort = null;
      }
    });
    builder.addCase(asyncGetSavedList.rejected, (state, action) => {
      state.statusRequest = RequestTypes.REJECTED;
    });
  },
});

// export const {} = savedListSlice.actions;
export default savedListSlice.reducer;
