import React from 'react';
import styled, { css } from 'styled-components';

import { color } from 'src/styles/color';
import { Title } from 'src/ui/Title';
import { AppText } from 'src/ui/AppText';
import { avatarName } from 'src/utils/avatarName';
import { formatDate } from 'src/utils/formatTime';
import { savedListTypes } from 'src/types/types';
import { Avatar } from 'src/ui/Avatar';
import { GrayBlockContainer } from 'src/layout/GrayBlockContainer';

const SavedItem: React.FC<SavedItemProps> = ({ item }) => {
  return (
    <Container>
      <Title tag="h4" type="h4" containerStyles={titleStyles}>
        {item.name ? item.name : 'No name'}
      </Title>
      <FilterWrapper>
        <AppText containerStyles={filterStyles}>Filters</AppText>
        <List>
          {item.filters ? (
            <>
              <Item>Travel Industry</Item>
              <Item>$500-$1B</Item>
            </>
          ) : (
            <div>No filters</div>
          )}
        </List>
      </FilterWrapper>
      <GrayBlockContainer>
        <AppText containerStyles={prospectsStyles}>
          № of Prospects Available
        </AppText>
        <AppText containerStyles={numberStyles}>
          {item.prospectsAvailable}
        </AppText>
      </GrayBlockContainer>
      <AuthorWrapper>
        <Wrapper>
          <Avatar
            name={avatarName(
              item.lastAuthor.firstName,
              item.lastAuthor.lastName
            )}
            containerStyles={authorAvatar}
          />
          <FlexWrapper>
            <AppText containerStyles={AuthorName}>
              {item.lastAuthor.firstName} {item.lastAuthor.lastName}
            </AppText>
            <AppText containerStyles={AuthorStatus}>
              {item.lastAuthor.role}
            </AppText>
          </FlexWrapper>
        </Wrapper>
        <FlexWrapper>
          <AppText containerStyles={lastActiveStyles}>Last Activity</AppText>
          <AppText containerStyles={dateStyles}>
            {formatDate(item.createdAt)}
          </AppText>
        </FlexWrapper>
      </AuthorWrapper>
    </Container>
  );
};

interface SavedItemProps {
  item: savedListTypes;
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 24px;
  width: 536px;
  min-height: 312px;
  padding: 24px;
  background-color: ${color.white};
  border-radius: 6px;
`;

const FilterWrapper = styled.div`
  margin-bottom: 24px;
`;

const List = styled.ul`
  margin-left: 5px;
  display: flex;
  flex-wrap: wrap;
`;

const Item = styled.li`
  padding: 6px 10px;
  margin-right: 8px;
  border-radius: 6px;
  border: 1px solid ${color.secondaryBlue};
  font-size: 12px;
  line-height: 150%;
`;

const AuthorWrapper = styled.div`
  margin-top: auto;
  display: flex;
  justify-content: space-between;
`;

const authorAvatar = css`
  width: 40px;
  height: 40px;
  margin-right: 12px;
`;

const Wrapper = styled.div`
  display: flex;
`;

const FlexWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const AuthorName = css`
  font-weight: 500;
  font-size: 12px;
  line-height: 150%;
`;

const AuthorStatus = css`
  font-weight: normal;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

const prospectsStyles = css`
  margin-bottom: 8px;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  text-align: center;
`;

const numberStyles = css`
  font-weight: 500;
  font-size: 24px;
  line-height: 148%;
  text-align: center;
`;

const filterStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 8px;
`;

const lastActiveStyles = css`
  font-size: 12px;
  line-height: 150%;
  text-align: right;
  color: ${color.darkGray};
`;

const dateStyles = css`
  font-size: 12px;
  line-height: 150%;
  text-align: right;
`;

const titleStyles = css`
  margin-top: 0;
  margin-bottom: 16px;
  padding-bottom: 9px;
  border-bottom: 1px solid ${color.line};
  text-align: start;
`;

export default SavedItem;
