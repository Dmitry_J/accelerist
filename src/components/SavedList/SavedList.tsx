import React from 'react';
import styled from 'styled-components';

import { SavedItem } from './components';
import { savedListTypes } from 'src/types/types';
import { useAppSelector } from 'src/store/hooks';
import { selectors } from 'src/store/ducks';

const SavedList: React.FC = () => {
  const itemsSavedList = useAppSelector(
    selectors.savedList.selectItemsSavedList
  );
  return (
    <Container>
      {itemsSavedList.length > 0 &&
        itemsSavedList.map((item: savedListTypes) => {
          return <SavedItem item={item} key={item.id} />;
        })}
    </Container>
  );
};

const Container = styled.div`
  margin-bottom: 16px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export default SavedList;
