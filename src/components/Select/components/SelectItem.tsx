import React, { useState } from 'react';
import { CheckBox } from 'src/ui/CheckBox';
import { css } from 'styled-components';

export const SelectItem: React.FC<SelectItemProps> = ({ item, handle }) => {
  const [isChecked, setIsChecked] = useState<boolean>(item.active);

  const handleChecked = () => {
    handle(item);
    setIsChecked(!isChecked);
  };

  return (
    <CheckBox
      title={item.title}
      isCheked={isChecked}
      onClick={handleChecked}
      labelStyles={labelStyles}
      containerStyles={containerStyles}
    />
  );
};

interface SelectItemProps {
  item: ItemType;
  handle: (item: { title: string; active: boolean }) => void;
}

type ItemType = {
  title: string;
  active: boolean;
};

const labelStyles = css`
  padding-left: 0;
  width: 100%;
  &::before {
    left: auto;
    right: 0;
  }
`;

const containerStyles = css`
  margin-bottom: 21px;
  &:last-child {
    margin-bottom: 0;
  }
`;
