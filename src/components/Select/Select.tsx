import React, { useState } from 'react';
import styled, { css, CSSProp } from 'styled-components';

import { SelectItem } from './components/SelectItem';
import Arrow from 'src/assets/images/icons/Arrow';
import { color } from 'src/styles/color';
import { trimString } from 'src/utils/formatWord';

const Select: React.FC<SelectProps> = ({ selectArray, handle }) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  let activeItem: string[] = [];

  selectArray.forEach((item) => {
    if (item.active) {
      activeItem.push(item.title);
    }
  });

  return (
    <Container>
      {activeItem.join(', ').length > 50 && isOpen && (
        <Tooltip>{activeItem.join(', ')}</Tooltip>
      )}
      <SelectWrapper
        $CSS={isOpen ? openSelectStyles : ''}
        onClick={() => setIsOpen(!isOpen)}
      >
        <div>
          {activeItem.length > 0 ? trimString(activeItem.join(', ')) : 'Select'}
        </div>
        <ArrowContainer $CSS={isOpen ? containerActiveStyles : ''}>
          <Arrow />
        </ArrowContainer>
      </SelectWrapper>
      {isOpen && (
        <List>
          {selectArray.map((item, i) => (
            <SelectItem item={item} key={i} handle={handle} />
          ))}
        </List>
      )}
    </Container>
  );
};

interface SelectProps {
  selectArray: SelectItemType[];
  handle: (item: { title: string; active: boolean }) => void;
}

interface SelectItemType {
  title: string;
  active: boolean;
}

type SelectWrapperType = {
  $CSS?: CSSProp;
};

type ArrowContainerType = {
  $CSS?: CSSProp;
};

const Container = styled.div`
  position: relative;
`;

const SelectWrapper = styled.div<SelectWrapperType>`
  position: relative;
  padding: 10px 16px;
  height: 46px;
  width: 100%;
  border: 1px solid ${color.line};
  border-radius: 6px;
  background-color: ${color.white};
  outline: none;
  font-size: 16px;
  line-height: 155%;
  cursor: pointer;
  ${({ $CSS }) => $CSS}
`;

const openSelectStyles = css`
  border-bottom: transparent;
  border-radius: 6px 6px 0px 0px;
`;

const List = styled.div`
  position: absolute;
  padding: 11px 16px;
  top: 100%;
  left: 0;
  width: 100%;
  max-height: 181px;
  overflow: auto;
  background-color: ${color.white};
  border: 1px solid ${color.line};
  border-top: none;
  border-radius: 0px 0px 6px 6px;
  z-index: 10;
`;

const ArrowContainer = styled.div<ArrowContainerType>`
  display: flex;
  position: absolute;
  top: 50%;
  right: 16px;
  transform: translateY(-50%);
  transition: 0.25s;
  ${({ $CSS }) => $CSS}
`;

const containerActiveStyles = css`
  transform: translateY(-50%) rotate(180deg);
`;

const Tooltip = styled.div`
  position: absolute;
  bottom: calc(100% + 8px);
  left: 0;
  max-width: 100%;
  padding: 16px 12px;
  box-shadow: 0px 20px 20px rgba(40, 31, 61, 0.02);
  border-radius: 8px;
  background-color: ${color.line};
  font-size: 12px;
  line-height: 150%;
  z-index: 10;
  &::before {
    content: '';
    position: absolute;
    bottom: -4px;
    left: 11px;
    width: 34px;
    height: 34px;
    box-shadow: 0px 20px 90px rgba(40, 31, 61, 0.11);
    border-radius: 4px;
    transform: rotate(45deg);
    background-color: ${color.line};
    z-index: -1;
  }
`;

export default Select;
