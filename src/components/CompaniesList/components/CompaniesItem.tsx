import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import { Link, useParams } from 'react-router-dom';

import { color } from 'src/styles/color';
import { CompanyItemTypes } from 'src/types/types';
import LogoPlug from 'src/assets/images/icons/LogoPlug';
import { AppText } from 'src/ui/AppText';
import { ButtonLike } from 'src/ui/ButtonLike';
import { formatWord } from 'src/utils/formatWord';
import { changeLikeCompany } from 'src/services/apiFunctions';
import { useAppDispatch } from 'src/store/hooks';
import { thunks } from 'src/store/ducks';
import { RequestTypes } from 'src/types/types';
import {
  successRequestMessage,
  errorRequestMessage,
} from 'src/utils/toastFunctions';
import { routes } from 'src/routes/routes';
import { Title } from 'src/ui/Title';
import { AppLink } from 'src/ui/AppLink';

const FavoritesItem: React.FC<FavoritesItemProps> = ({ company }) => {
  const [isLoading, setIsLoading] = useState<string | undefined>('');
  const dispatch = useAppDispatch();
  const routerParams: RouterParams = useParams();

  let page: number = 1;

  if (routerParams.page) page = Number(routerParams.page);

  const handleLikeCompany = (id: string, like: boolean) => {
    setIsLoading(RequestTypes.PENDING);
    changeLikeCompany({
      id,
      like,
    })
      .then((res) => {
        dispatch(
          thunks.companies.asyncGetFavoritesCompanies({
            page,
            limit: 15,
          })
        );
        successRequestMessage('Company update success!');
        setIsLoading('');
      })
      .catch((e) => {
        console.warn(e);
        errorRequestMessage(e);
        setIsLoading('');
      });
  };

  return (
    <Container>
      <LogoContainer>
        <Logo>
          {company.logo ? (
            <img src={company.logo} alt={`Logo: ${company.name}`} />
          ) : (
            <LogoPlug />
          )}
        </Logo>
        <RankWrapper>
          <AppText tag="span" containerStyles={rankStyles}>
            Priority Ranking
          </AppText>
          <AppText tag="span" containerStyles={rankScore}>
            {company.score}
          </AppText>
        </RankWrapper>
      </LogoContainer>
      <InfoBlock>
        <InfoWrapper>
          <div>
            <Title type="h4" tag="h4" containerStyles={companyNameStyles}>
              <Link to={`${routes.COMPANY}/${company.id}`}>{company.name}</Link>
            </Title>
            <AppText containerStyles={textInfoStyles}>{company.street}</AppText>
            <AppText containerStyles={textInfoStyles}>{company.phone}</AppText>
          </div>
          <Wrapper>
            <CSRBlock>
              <AppText containerStyles={textStyles}>CSR Focus</AppText>
              <List>
                {company.crsFocus.length > 0 ? (
                  company.crsFocus.map((csrItem) => {
                    return <ListItem>{csrItem}</ListItem>;
                  })
                ) : (
                  <AppText containerStyles={subTextStyles}>
                    No information
                  </AppText>
                )}
              </List>
            </CSRBlock>
            <RevenueBlock>
              <AppText containerStyles={textStyles}>Revenue</AppText>
              <AppText containerStyles={subTextStyles}>
                $ {formatWord(company.revenue)}
              </AppText>
            </RevenueBlock>
          </Wrapper>
        </InfoWrapper>
        <ButtonGroup>
          <ButtonLike
            like={company.like}
            containerStyles={likeStyles}
            onClick={() => handleLikeCompany(company.id, company.like)}
            loading={isLoading}
          />
          <AppLink to={`${routes.COMPANY}/${company.id}`}>Profile</AppLink>
        </ButtonGroup>
      </InfoBlock>
    </Container>
  );
};

interface FavoritesItemProps {
  company: CompanyItemTypes;
}

interface RouterParams {
  page: string;
}

const InfoBlock = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;
`;
const ButtonGroup = styled.div`
  margin-top: 24px;
  display: flex;
`;
const InfoWrapper = styled.div`
  flex-grow: 1;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  border-bottom: 1px solid ${color.line};
`;

const CSRBlock = styled.div`
  padding-bottom: 12px;
`;

const RevenueBlock = styled.div`
  padding-bottom: 12px;
  padding-left: 20px;
  border-left: 1px solid ${color.line};
  text-align: end;
`;

const likeStyles = css`
  margin-right: 8px;
`;

const companyNameStyles = css`
  margin-bottom: 12px;
`;

const textInfoStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 4px;
  &:last-child {
    margin-bottom: 0;
  }
`;

const subTextStyles = css`
  font-weight: 500;
  font-size: 12px;
  line-height: 150%;
`;

const Container = styled.div`
  display: flex;
  margin-bottom: 24px;
  width: 536px;
  min-height: 268px;
  padding: 24px 32px;
  background-color: ${color.white};
  border-radius: 6px;
`;

const LogoContainer = styled.div`
  width: 168px;
  border: 1px solid ${color.line};
  box-sizing: border-box;
  border-radius: 6px;
  display: flex;
  flex-direction: column;
  margin-right: 16px;
`;

const Logo = styled.div`
  flex-grow: 1;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const RankWrapper = styled.div`
  padding: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  border-top: 1px solid ${color.line};
`;

const List = styled.ul`
  display: flex;
`;

const ListItem = styled.li`
  position: relative;
  margin-right: 16px;
  font-weight: 500;
  font-size: 12px;
  line-height: 150%;
  &::before {
    content: '';
    position: absolute;
    height: 4px;
    width: 4px;
    top: 50%;
    transform: translateY(-50%);
    right: -10px;
    background-color: ${color.grayC4};
    border-radius: 50%;
  }
  &:last-child {
    margin-right: 0;
    &::before {
      content: none;
    }
  }
`;

const rankStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 2px;
`;

const rankScore = css`
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
`;

const textStyles = css`
  margin-bottom: 4px;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

export default FavoritesItem;
