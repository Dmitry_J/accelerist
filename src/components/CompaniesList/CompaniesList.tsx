import React from 'react';
import styled from 'styled-components';

import { CompaniesItem } from './components';
import { CompanyItemTypes } from 'src/types/types';

const CompaniesList: React.FC<CompaniesListProps> = ({ companies }) => {
  return (
    <Container>
      {companies.length > 0 &&
        companies.map((item: CompanyItemTypes) => (
          <CompaniesItem key={item.id} company={item} />
        ))}
    </Container>
  );
};

interface CompaniesListProps {
  companies: CompanyItemTypes[];
}

const Container = styled.div`
  margin-bottom: 16px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

export default CompaniesList;
