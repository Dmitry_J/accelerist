import React from 'react';
import styled, { css } from 'styled-components';

import { AppText } from 'src/ui/AppText';
import { Button } from 'src/ui/Button';
import { ButtonLike } from 'src/ui/ButtonLike';
import TwitterIcon from 'src/assets/images/icons/TwitterIcon';
import FacebookIcon from 'src/assets/images/icons/FacebookIcon';
import LinkedinIcon from 'src/assets/images/icons/LinkedinIcon';
import { color } from 'src/styles/color';
import { Avatar } from 'src/ui/Avatar';
import { Title } from 'src/ui/Title';

const TitleCompanySection: React.FC<TitleCompanySectionProps> = ({
  name,
  logo,
  like,
  primaryIndustry,
}) => {
  return (
    <Container>
      <FlexBlock>
        <Avatar
          name={name[0].toUpperCase()}
          avatar={logo}
          containerStyles={avatarStyles}
        />
        <Block>
          <TitleBlock>
            <Title type="h3" tag="h2" containerStyles={titleStyles}>
              {name}
            </Title>
            <ButtonLike like={like} containerStyles={likeBtnStyles} />
          </TitleBlock>
          <AppText containerStyles={subTitleTextStyles}>
            {primaryIndustry.map((item: string) => item)}
          </AppText>
          <SocialContainer>
            <SocialLink href="twitter">
              <TwitterIcon />
            </SocialLink>
            <SocialLink href="facebook">
              <FacebookIcon />
            </SocialLink>
            <SocialLink href="linkedin">
              <LinkedinIcon />
            </SocialLink>
          </SocialContainer>
        </Block>
      </FlexBlock>
      <Button text="Block" containerStyles={blockBtnStyles} />
    </Container>
  );
};

interface TitleCompanySectionProps {
  name: string;
  logo: string | undefined;
  like: boolean;
  primaryIndustry: string[];
}

const FlexBlock = styled.div`
  display: flex;
`;

const Container = styled.div`
  padding: 40px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: ${color.grayF2};
  border-radius: 6px 6px 0px 0px;
`;

const Block = styled.div`
  display: flex;
  flex-direction: column;
`;

const TitleBlock = styled.div`
  display: flex;
  align-items: center;
  margin-bottom: 4px;
`;

const avatarStyles = css`
  height: 100px;
  width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: ${color.white};
  border-radius: 8px;
  margin-right: 24px;
  border: 1px solid ${color.line};
  font-weight: 500;
  font-size: 32px;
  line-height: 150%;
`;

const titleStyles = css`
  margin-right: 8px;
`;

const subTitleTextStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

const SocialContainer = styled.div`
  display: flex;
  align-items: end;
  flex-grow: 1;
`;

const blockBtnStyles = css`
  width: auto;
  height: auto;
  background: ${color.white};
  border: 1px solid ${color.line};
  box-sizing: border-box;
  border-radius: 6px;
  padding: 9px 37px;
  font-family: 'Rubik';
  font-size: 12px;
  line-height: 150%;
  text-align: center;
  color: ${color.red};
`;

const SocialLink = styled.a`
  margin-right: 12px;
  &:last-child {
    margin-right: 0;
  }
`;

const likeBtnStyles = css`
  background-color: transparent;
  border: none;
  &:hover {
    border: none;
  }
`;

export default TitleCompanySection;
