import React from 'react';
import styled from 'styled-components';

import { color } from 'src/styles/color';

export const TabItem: React.FC<TabItemProps> = ({
  title,
  activeTab,
  onClick,
}) => {
  return (
    <Root $active={activeTab === title} onClick={onClick}>
      {title}
    </Root>
  );
};

interface TabItemProps {
  title: string;
  activeTab?: string;
  onClick?: () => void;
}

type ItemType = {
  $active?: boolean;
};

const Root = styled.div<ItemType>`
  display: flex;
  align-items: center;
  justify-content: center;
  transition: 0.25s;
  background: ${({ $active }) => ($active ? color.secondaryBlue : '')};
  border-radius: 6px;
  flex-grow: 1;
  cursor: pointer;
`;
