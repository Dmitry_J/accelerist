import React, { useState } from 'react';
import { color } from 'src/styles/color';
import styled, { CSSProp } from 'styled-components';

import { TabItem } from './components/TabItem';

const Tabs: React.FC<TabsProps> = ({ tabItems, containerStyles = {} }) => {
  const [active, setActive] = useState<string>(tabItems[0]);

  return (
    <Container $CSS={containerStyles}>
      {tabItems.map((item, i) => (
        <TabItem
          title={item}
          key={i}
          activeTab={active}
          onClick={() => setActive(item)}
        />
      ))}
    </Container>
  );
};

interface TabsProps {
  tabItems: string[];
  containerStyles?: CSSProp;
}

type ContainerType = {
  $CSS?: CSSProp;
};

const Container = styled.div<ContainerType>`
  display: flex;
  justify-content: space-between;
  padding: 2px;
  background-color: ${color.bgF8};
  border-radius: 6px;
  height: 40px;
  ${({ $CSS }) => $CSS}
`;

export default Tabs;
