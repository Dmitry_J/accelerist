import React from 'react';
import styled, { css } from 'styled-components';

import { AppText } from 'src/ui/AppText';
import { color } from 'src/styles/color';
import { SortItem } from './components';
import { sort } from 'src/utils/sortArray';

const SortProspects: React.FC<SortProspectsProps> = ({ handleSort }) => {
  return (
    <Container>
      <AppText containerStyles={textStyles}>Sort by</AppText>
      <List>
        {sort.map((item) => {
          return (
            <SortItem
              sortItem={item}
              key={item.sortName}
              onClick={() => handleSort(item.sortName)}
            />
          );
        })}
      </List>
    </Container>
  );
};

interface SortProspectsProps {
  handleSort: (sort: string) => void;
}

const Container = styled.div`
  display: flex;
`;

const List = styled.ul`
  display: flex;
`;

const textStyles = css`
  margin-right: 26px;
  margin-bottom: 0;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

export default SortProspects;
