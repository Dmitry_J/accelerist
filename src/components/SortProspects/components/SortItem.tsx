import React, { HtmlHTMLAttributes } from 'react';
import styled, { CSSProp } from 'styled-components';

import { color } from 'src/styles/color';
import { selectors } from 'src/store/ducks';
import { useAppSelector } from 'src/store/hooks';
import { SortTypes } from 'src/utils/sortArray';

const SortItem: React.FC<SortItemProps> = ({
  containerStyles = {},
  sortItem,
  ...props
}) => {
  const sortSavedList = useAppSelector(selectors.savedList.selectSortSavedList);
  if (sortSavedList === sortItem.sortName) containerStyles = active;

  return (
    <ListItem $CSS={containerStyles} {...props}>
      {sortItem.title}
    </ListItem>
  );
};

interface SortItemProps extends HtmlHTMLAttributes<HTMLLIElement> {
  sortItem: SortTypes;
  containerStyles?: CSSProp;
}

interface LiTypes {
  $CSS?: CSSProp;
}

const ListItem = styled.li<LiTypes>`
  padding-bottom: 2px;
  position: relative;
  margin-right: 22px;
  font-size: 12px;
  line-height: 150%;
  cursor: pointer;
  transition: 0.25s;
  &::before {
    content: '';
    position: absolute;
    bottom: 0;
    width: 0;
    left: 50%;
    transform: translateX(-50%);
    height: 2px;
    background-color: ${color.blue};
  }
  &:last-child {
    margin-right: 0;
  }
  &:hover {
    &::before {
      width: 100%;
    }
  }
  ${({ $CSS }) => $CSS};
`;

const active = `
  &::before {
    width: 100%
  }
`;

export default SortItem;
