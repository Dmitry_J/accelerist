import React, { useState, useEffect } from 'react';
import { Form, Field, FormProps } from 'react-final-form';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import * as PersistTypes from 'redux-persist/es/types';

import { ToastContainer } from 'react-toastify';
import { routes } from 'src/routes/routes';
import { thunks, selectors, actions } from 'src/store/ducks';
import { useAppDispatch, useAppSelector } from 'src/store/hooks';
import { color } from 'src/styles/color';
import { Button } from 'src/ui/Button';
import { CheckBox } from 'src/ui/CheckBox';
import { errorRequestMessage } from 'src/utils/toastFunctions';
import { validateForm } from 'src/utils/validateFunc';
import { InputField } from '../InputField';
import { AppText } from 'src/ui/AppText';

const SignInForm: React.FC<SignInFormProps> = ({ persistor }) => {
  const [isChecked, setIsChecked] = useState<boolean>(false);
  const dispatch = useAppDispatch();
  const statusRequest = useAppSelector(selectors.user.selectStatus);
  const errorMessage = useAppSelector(selectors.user.selectError);

  const handleChecked = () => setIsChecked(!isChecked);

  const onSubmit = (values: FormProps) => {
    dispatch(
      thunks.user.signInAction({
        email: values.email,
        password: values.password,
      })
    );
  };

  useEffect(() => {
    if (errorMessage) {
      errorRequestMessage(errorMessage);
      dispatch(actions.user.resetError());
    }
  });

  return (
    <>
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, submitting, pristine }) => (
          <form onSubmit={handleSubmit}>
            <Field
              name="email"
              validate={validateForm.composeValidators(
                validateForm.required,
                validateForm.correctMail
              )}
              render={({ ...props }) => {
                return (
                  <InputField
                    ladel="Email"
                    {...props}
                    placeholderText="Enter email"
                  />
                );
              }}
            />

            <Field
              name="password"
              validate={validateForm.composeValidators(
                validateForm.required,
                validateForm.minValue()
              )}
              render={({ ...props }) => {
                return (
                  <InputField
                    ladel="Password"
                    placeholderText="Enter password"
                    {...props}
                    containerStyles={inputContainerStyles}
                  />
                );
              }}
            />

            <Wrapper>
              <CheckBoxWrapper>
                <CheckBox
                  isCheked={isChecked}
                  onClick={handleChecked}
                  title="Remember"
                />
              </CheckBoxWrapper>
              <AppText containerStyles={textRightStyles}>
                <Link to={routes.RESET}>Forgot Password?</Link>
              </AppText>
            </Wrapper>

            <Button
              containerStyles={buttonStyles}
              type="submit"
              text="Submit"
              loading={statusRequest}
              spinColor={color.white}
              disabled={submitting || pristine}
            />
          </form>
        )}
      />
      <ToastContainer
        position="top-right"
        autoClose={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
      />
    </>
  );
};

interface SignInFormProps {
  persistor?: PersistTypes.Persistor;
}

const inputContainerStyles = `
  margin-bottom: 0;
`;

const buttonStyles = `
  margin-bottom: 32px;
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-top: 24px;
  margin-bottom: 56px;
`;

const CheckBoxWrapper = styled.div`
  display: flex;
  align-items: center;
`;

const textRightStyles = css`
  position: relative;
  transition: 0.25s;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

export default SignInForm;
