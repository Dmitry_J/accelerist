import React from 'react';
import styled, { css } from 'styled-components';

import { AppText } from 'src/ui/AppText';
import { Title } from 'src/ui/Title';

const DescriptionSection: React.FC<DescriptionSectionProps> = ({
  description,
}) => {
  return (
    <Container>
      <Title type="h4" tag="h4" containerStyles={titleStyles}>
        Description
      </Title>
      <AppText containerStyles={descStyles}>{description}</AppText>
      <Title type="h4" tag="h4" containerStyles={titleStyles}>
        Products & Brand Descriptions
      </Title>
      <AppText containerStyles={descStyles}>{description}</AppText>
      <Title type="h4" tag="h4" containerStyles={titleStyles}>
        Structure
      </Title>
      <AppText containerStyles={descStyles}>Sole proprietorship</AppText>
    </Container>
  );
};

interface DescriptionSectionProps {
  description: string;
}

const Container = styled.div`
  margin-bottom: 32px;
`;

const titleStyles = css`
  margin-bottom: 16px;
`;

const descStyles = css`
  font-size: 16px;
  line-height: 155%;
  margin-bottom: 24px;
  &:last-child {
    margin-bottom: 0;
  }
`;

export default DescriptionSection;
