import React from 'react';
import styled from 'styled-components';
import { Form, Field, FormProps } from 'react-final-form';

import { Input } from 'src/ui/Input';
import { color } from 'src/styles/color';
import { SearchButton } from 'src/ui/SearchButton';

const Search: React.FC = () => {
  const onSubmit = (values: FormProps) => {
    console.log(values);
  };
  return (
    <Form
      onSubmit={onSubmit}
      render={({ handleSubmit }) => (
        <FormSearch onSubmit={handleSubmit}>
          <Field
            name="search"
            render={({ input }) => {
              return (
                <Input
                  placeholder="Search"
                  containerStyled={inputStyles}
                  {...input}
                />
              );
            }}
          />
          <SearchButton type="submit" />
        </FormSearch>
      )}
    />
  );
};

const FormSearch = styled.form`
  margin-right: 40px;
  position: relative;
  height: 36px;
  width: 365px;
`;

const inputStyles = `
height: 100%;
padding-left: 24px;
padding-right: 50px;
background-color: ${color.searchBG};
border: 1px solid transparent;
&::placeholder {
    font-family: 'Rubik';
    font-weight: normal;
    font-size: 12px;
    line-height: 150%;
    color: ${color.darkGray};
}
`;

export default Search;
