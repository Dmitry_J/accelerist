import React from 'react';
import styled from 'styled-components';

import { Title } from 'src/ui/Title';

import companyLogo1 from 'src/assets/images/companyLogo1.png';
import companyLogo2 from 'src/assets/images/companyLogo2.png';
import companyLogo3 from 'src/assets/images/companyLogo3.png';
import companyLogo4 from 'src/assets/images/companyLogo4.png';

const TopMatchedSection: React.FC = () => {
  return (
    <Wrapper>
      <Title type="h4" tag="h4">
        Top Matched
      </Title>
      <ImageBlock src={companyLogo1} alt="Logo: Samsung" />
      <ImageBlock src={companyLogo2} alt="Logo: Nasa" />
      <ImageBlock src={companyLogo3} alt="Logo: Raiffeisen Bank" />
      <ImageBlock src={companyLogo4} alt="Logo: Greenpeace" />
    </Wrapper>
  );
};

const Wrapper = styled.div`
  margin-bottom: 4px;
`;

const ImageBlock = styled.img`
  height: 83px;
  width: 83px;
  margin-right: 12px;
  &:last-child {
    margin-right: 0;
  }
`;

export default TopMatchedSection;
