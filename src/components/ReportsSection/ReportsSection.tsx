import React from 'react';
import styled, { css } from 'styled-components';

import { SectionTitle } from 'src/ui/SectionTitle';
import { color } from 'src/styles/color';
import { TopMatchedSection } from 'src/components/TopMatchedSection';
import { Avatar } from 'src/ui/Avatar';
import { GrayBlockContainer } from 'src/layout/GrayBlockContainer';
import { Title } from 'src/ui/Title';
import { AppText } from 'src/ui/AppText';

const ReportsSection: React.FC<ReportsSectionProps> = ({
  pitchCount,
  searchCount,
}) => {
  return (
    <Container>
      <SectionTitle title="Reports" />
      <Block>
        <Wrapper>
          <ContentBlock>
            <Title type="h4" tag="h4">
              Search Sessions
            </Title>
            <GrayBlockContainer>
              <AppText containerStyles={totalStyles}>Total</AppText>
              <AppText containerStyles={numberStyles}>
                {searchCount ? searchCount : '0'}
              </AppText>
            </GrayBlockContainer>
          </ContentBlock>
          <ContentBlock>
            <Title type="h4" tag="h4">
              Sent Pitches
            </Title>
            <GrayBlockContainer>
              <AppText containerStyles={totalStyles}>Company</AppText>
              <AppText containerStyles={numberStyles}>
                {pitchCount ? pitchCount : '0'}
              </AppText>
            </GrayBlockContainer>
          </ContentBlock>
        </Wrapper>
        <TopMatchedSection />
        <div>
          <Title type="h4" tag="h4">
            Last Login
          </Title>
          <ul>
            <ListItem>
              <Avatar name="TT" />
              <NameContainer>
                <AppText containerStyles={nameStyles}>Test test</AppText>
                <AppText containerStyles={dateStyles}>date</AppText>
              </NameContainer>
            </ListItem>
            <ListItem>
              <Avatar name="TT" />
              <NameContainer>
                <AppText containerStyles={nameStyles}>Test test</AppText>
                <AppText containerStyles={dateStyles}>date</AppText>
              </NameContainer>
            </ListItem>
            <ListItem>
              <Avatar name="TT" />
              <NameContainer>
                <AppText containerStyles={nameStyles}>Test test</AppText>
                <AppText containerStyles={dateStyles}>date</AppText>
              </NameContainer>
            </ListItem>
          </ul>
        </div>
      </Block>
    </Container>
  );
};

interface ReportsSectionProps {
  pitchCount: number | null;
  searchCount: number | null;
}

const Container = styled.div`
  width: 536px;
`;

const Block = styled.div`
  padding: 24px;
  border-radius: 6px;
  background-color: ${color.white};
`;

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 24px;
`;

const ContentBlock = styled.div`
  width: 235px;
`;

const ListItem = styled.li`
  display: flex;
  align-items: center;
`;

const NameContainer = styled.div`
  padding: 15px 0;
  position: relative;
  display: flex;
  justify-content: space-between;
  flex-grow: 1;
  align-items: center;
  border-bottom: 1px solid ${color.grayEEE};
`;

const totalStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 8px;
`;

const numberStyles = css`
  font-weight: 500;
  font-size: 24px;
  line-height: 148%;
`;

const nameStyles = css`
  font-weight: 500;
  font-size: 12px;
  line-height: 150%;
`;

const dateStyles = css`
  font-size: 12px;
  line-height: 150%;
  text-align: right;
  color: ${color.darkGray};
`;

export default ReportsSection;
