import React from 'react';
import styled, { css } from 'styled-components';

import { color } from 'src/styles/color';
import { Title } from 'src/ui/Title';
import { AppText } from 'src/ui/AppText';
import scoop from 'src/assets/images/scoop.png';
import news from 'src/assets/images/new.png';

const NewsSection: React.FC<NewSectionProps> = () => {
  return (
    <Container>
      <Title containerStyles={titleStyles}>Scoop</Title>
      <ScoopBlock>
        <ImageContainer>
          <ImageOverlay />
          <List>
            <ListItem>Technology</ListItem>
            <ListItem>Microsoft</ListItem>
            <ListItem>Awards</ListItem>
          </List>
        </ImageContainer>
        <Title type="h4" tag="h4">
          The Decline of Clothing Retail
        </Title>
        <AppText containerStyles={textStyles}>
          The apparel industry’s appeal has been dwindling for a while: “In
          1977, clothing industry’s appeal
        </AppText>
        <AppText containerStyles={dateStyles}>2 Sep 2020, 13:23</AppText>
      </ScoopBlock>

      <div>
        <Title containerStyles={titleStyles}>News</Title>
        <ul>
          <ListNews>
            <ImageNewsContainer />
            <NewsWrapper>
              <div>
                <NewsTitle>The Decline of Clothing Retail</NewsTitle>
                <AppText containerStyles={newsTextStyles}>
                  The apparel industry’s appeal has been dwindling for a while:
                  “In 1977, clothing industry’s appeal
                </AppText>
              </div>
              <AppText containerStyles={dateStyles}>2 Sep 2020, 13:23</AppText>
            </NewsWrapper>
          </ListNews>

          <ListNews>
            <ImageNewsContainer />
            <NewsWrapper>
              <div>
                <NewsTitle>The Decline of Clothing Retail</NewsTitle>
                <AppText containerStyles={newsTextStyles}>
                  The apparel industry’s appeal has been dwindling for a while:
                  “In 1977, clothing industry’s appeal
                </AppText>
              </div>
              <AppText containerStyles={dateStyles}>2 Sep 2020, 13:23</AppText>
            </NewsWrapper>
          </ListNews>

          <ListNews>
            <ImageNewsContainer />
            <NewsWrapper>
              <div>
                <NewsTitle>The Decline of Clothing Retail</NewsTitle>
                <AppText containerStyles={newsTextStyles}>
                  The apparel industry’s appeal has been dwindling for a while:
                  “In 1977, clothing industry’s appeal
                </AppText>
              </div>
              <AppText containerStyles={dateStyles}>2 Sep 2020, 13:23</AppText>
            </NewsWrapper>
          </ListNews>
        </ul>
      </div>
    </Container>
  );
};

interface NewSectionProps {}

const Container = styled.div`
  position: relative;
  padding: 32px 40px;
  background-color: ${color.white};
  min-width: 366px;
  max-width: 366px;
  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    width: 1px;
    height: calc(100% - 52px);
    background-color: ${color.line};
  }
`;

const titleStyles = css`
  text-align: start;
  font-size: 16px;
  line-height: 145%;
  margin-bottom: 16px;
`;

const ScoopBlock = styled.div`
  margin-bottom: 24px;
`;

const ImageContainer = styled.div`
  position: relative;
  display: flex;
  align-items: end;
  padding: 8px;
  margin-bottom: 16px;
  height: 187px;
  background-size: cover;
  border-radius: 6px;
  background-image: url(${scoop});
  background-repeat: no-repeat;
  background-position: -125px;
  overflow: hidden;
`;

const ImageOverlay = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: linear-gradient(
    180deg,
    rgba(18, 36, 52, 0) 63.25%,
    #122434 88.97%
  );
`;

const List = styled.ul`
  position: relative;
  display: flex;
`;

const ListItem = styled.li`
  position: relative;
  font-size: 12px;
  line-height: 150%;
  color: #e8e8e8;
  margin-right: 20px;
  &::before {
    content: '';
    position: absolute;
    width: 4px;
    height: 4px;
    top: 50%;
    transform: translateY(-50%);
    right: -12px;
    border-radius: 50%;
    background-color: ${color.white};
  }
  &:last-child {
    margin-right: 0;
    &::before {
      content: none;
    }
  }
`;

const textStyles = css`
  font-size: 12px;
  line-height: 150%;
  margin-bottom: 8px;
`;

const dateStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

const newsTextStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

const ImageNewsContainer = styled.div`
  width: 72px;
  min-width: 72px;
  height: 110px;
  margin-right: 16px;
  border-radius: 8px;
  background-image: url(${news});
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  overflow: hidden;
`;

const NewsTitle = styled.h4`
  font-weight: 500;
  font-size: 12px;
  line-height: 150%;
  margin-top: 0;
  margin-bottom: 8px;
`;

const ListNews = styled.li`
  display: flex;
  margin-bottom: 32px;
  &:last-child {
    margin-bottom: 0;
    div {
      &::before {
        content: none;
      }
    }
  }
`;

const NewsWrapper = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  &::before {
    content: '';
    position: absolute;
    left: 0;
    bottom: -16px;
    width: 100%;
    height: 1px;
    background-color: ${color.line};
  }
`;

export default NewsSection;
