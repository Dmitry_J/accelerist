import React, { useState } from 'react';
import styled, { css, CSSProp } from 'styled-components';
import { FieldRenderProps } from 'react-final-form';

import { Input } from 'src/ui/Input';
import { color } from 'src/styles/color';
import { IconHidePassword } from 'src/ui/IconHidePassword';
import { AppText } from 'src/ui/AppText';

const InputField: React.FC<InputFieldProps> = ({
  ladel,
  meta,
  input,
  placeholderText,
  containerStyles = {},
  inputStyles = {},
}) => {
  const [isHidePassword, setIsHidePassword] = useState<boolean>(true);

  const handleCheckPassword = () => setIsHidePassword(!isHidePassword);
  return (
    <Root $CSS={containerStyles}>
      <AppText type="labe" tag="label">
        {ladel}
      </AppText>
      {input.name === 'password' ? (
        <InputPasswordContainer>
          <Input
            type={isHidePassword ? 'password' : 'text'}
            {...input}
            placeholder={placeholderText}
            containerStyled={[
              meta.touched && meta.error && errorInputStyles,
              inputStyles,
            ]}
          />
          <IconHidePassword
            isHide={isHidePassword}
            onClick={handleCheckPassword}
          />
        </InputPasswordContainer>
      ) : (
        <Input
          type="text"
          {...input}
          placeholder={placeholderText}
          containerStyled={[
            meta.touched && meta.error && errorInputStyles,
            inputStyles,
          ]}
        />
      )}
      {meta.touched && (meta.error || meta.submitError) && (
        <AppText tag="span" containerStyles={errorTextStyles}>
          {meta.error || meta.submitError}
        </AppText>
      )}
    </Root>
  );
};

interface InputFieldProps extends FieldRenderProps<string> {
  ladel?: string;
  containerStyles?: CSSProp;
  inputStyles?: CSSProp;
  placeholderText?: string;
}

type RootType = {
  $CSS: CSSProp;
};

const Root = styled.div<RootType>`
  position: relative;
  display: flex;
  flex-direction: column;
  margin-bottom: 24px;
  ${({ $CSS }) => $CSS}
`;

const errorInputStyles = css`
  background-color: ${color.lightRed};
  border: 1px solid ${color.red};
`;

const errorTextStyles = css`
  position: absolute;
  display: inline-block;
  bottom: -20px;
  font-size: 12px;
  line-height: 150%;
  color: ${color.red};
`;

const InputPasswordContainer = styled.div`
  position: relative;
`;

export default InputField;
