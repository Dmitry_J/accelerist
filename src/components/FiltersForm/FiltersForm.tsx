import React, { useState } from 'react';
import styled, { css } from 'styled-components';
import { Form, Field, FormProps } from 'react-final-form';

import { color } from 'src/styles/color';
import { Title } from 'src/ui/Title';
import { InputField } from '../InputField';
import { IndustrySelect } from './components/IndustrySelect';
import {
  geographicLocation,
  industry,
  sdgGoals,
  cdrFocus,
  gender,
  relations,
  householdIncome,
  ethnicity,
} from 'src/utils/selectArray';
import { Select } from '../Select';
import { Tabs } from '../Tabs';
import { AppInputRange } from 'src/ui/AppInputRange';
import { valueText } from 'src/utils/formatWord';
import { Button } from 'src/ui/Button';
import { AppText } from 'src/ui/AppText';

const FiltersForm: React.FC<FiltersProps> = () => {
  const [revenue, setRevenue] = useState<number[]>([0, 100]);
  const [age, setAge] = useState<number[]>([0, 100]);
  const [industrySelect, setIndustrySelect] = useState<string[]>([]);
  const [cdrFocusArr, setCdrFocusArr] =
    useState<{ title: string; active: boolean }[]>(cdrFocus);
  const [sdgGoalsArr, setSdgGoalsArr] =
    useState<{ title: string; active: boolean }[]>(sdgGoals);
  const [householdIncomeArr, setHouseholdIncome] =
    useState<{ title: string; active: boolean }[]>(householdIncome);
  const [ethnicityArr, setEthnicityArr] =
    useState<{ title: string; active: boolean }[]>(ethnicity);

  const handleIndustry = (val: string, active: boolean) => {
    if (active) {
      setIndustrySelect((prev) => [...prev, val]);
    } else {
      setIndustrySelect((prev) => prev.filter((item) => item !== val));
    }
  };

  const handleCdrFocusArr = (item: { title: string; active: boolean }) => {
    const newArr = cdrFocusArr.map((arrItem) => {
      if (item.title === arrItem.title) {
        arrItem.active = !arrItem.active;
        return arrItem;
      } else {
        return arrItem;
      }
    });
    setCdrFocusArr(newArr);
  };

  const handleHouseholdIncomeArr = (item: {
    title: string;
    active: boolean;
  }) => {
    const newArr = householdIncomeArr.map((arrItem) => {
      if (item.title === arrItem.title) {
        arrItem.active = !arrItem.active;
        return arrItem;
      } else {
        return arrItem;
      }
    });
    setHouseholdIncome(newArr);
  };

  const handleSdgGoalsArr = (item: { title: string; active: boolean }) => {
    const newArr = sdgGoalsArr.map((arrItem) => {
      if (item.title === arrItem.title) {
        arrItem.active = !arrItem.active;
        return arrItem;
      } else {
        return arrItem;
      }
    });
    setSdgGoalsArr(newArr);
  };

  const handleEthnicityArr = (item: { title: string; active: boolean }) => {
    const newArr = ethnicityArr.map((arrItem) => {
      if (item.title === arrItem.title) {
        arrItem.active = !arrItem.active;
        return arrItem;
      } else {
        return arrItem;
      }
    });
    setEthnicityArr(newArr);
  };

  const handleChangeRevenue = (event: Event, newValue: number | number[]) => {
    setRevenue(newValue as number[]);
  };

  const handleChangeAge = (event: Event, newValue: number | number[]) => {
    setAge(newValue as number[]);
  };

  const onSubmit = (values: FormProps) => {
    console.log(values);
  };

  return (
    <Container>
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
          <form onSubmit={handleSubmit}>
            <Title type="h3" tag="h3" containerStyles={titleStyles}>
              Filters
            </Title>

            <Tabs
              tabItems={['Advanced', 'Customize']}
              containerStyles={tabsStyles}
            />

            <section>
              <Title type="h4" tag="h4">
                Company
              </Title>

              <InputContainer>
                <Wrapper>
                  <Field
                    name="industry"
                    render={({ ...props }) => {
                      return (
                        <InputField
                          ladel="Industry"
                          placeholderText="Search"
                          {...props}
                        />
                      );
                    }}
                  />
                  <SelectContainer>
                    {industry.map((item, id) => (
                      <IndustrySelect
                        title={item.title}
                        key={id}
                        handle={handleIndustry}
                        active={item.active}
                      />
                    ))}
                  </SelectContainer>
                </Wrapper>

                <Wrapper>
                  <Field
                    name="geographicLocation"
                    render={({ ...props }) => {
                      return (
                        <InputField
                          ladel="Geographic Location"
                          placeholderText="Search"
                          {...props}
                        />
                      );
                    }}
                  />
                  <SelectContainer>
                    {geographicLocation.map((item, id) => (
                      <IndustrySelect title={item} key={id} />
                    ))}
                  </SelectContainer>
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    SDG Goals
                  </AppText>
                  <Select selectArray={sdgGoals} handle={handleSdgGoalsArr} />
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    CDR Focus
                  </AppText>
                  <Select selectArray={cdrFocus} handle={handleCdrFocusArr} />
                </Wrapper>

                <Wrapper>
                  <Field
                    name="totalAnnualContributions"
                    render={({ ...props }) => {
                      return (
                        <InputField
                          ladel="Total Annual Contributions"
                          placeholderText="Search"
                          {...props}
                        />
                      );
                    }}
                  />
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    Revenue
                  </AppText>
                  <RangeContainer>
                    <AppInputRange
                      getAriaLabel={() => 'Revenue'}
                      step={0.5}
                      value={revenue}
                      valueLabelDisplay="on"
                      valueLabelFormat={valueText}
                      onChange={handleChangeRevenue}
                    />
                  </RangeContainer>
                </Wrapper>
              </InputContainer>
            </section>

            <section>
              <Title type="h4" tag="h4">
                Customer Demographics
              </Title>
              <InputContainer>
                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    Gender
                  </AppText>
                  <Tabs tabItems={gender} />
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    Relations
                  </AppText>
                  <Tabs tabItems={relations} />
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    Household Income
                  </AppText>
                  <Select
                    selectArray={householdIncome}
                    handle={handleHouseholdIncomeArr}
                  />
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    Ethnicity
                  </AppText>
                  <Select selectArray={ethnicity} handle={handleEthnicityArr} />
                </Wrapper>

                <Wrapper>
                  <AppText
                    type="label"
                    tag="label"
                    containerStyles={labelStyles}
                  >
                    Age
                  </AppText>
                  <RangeContainer>
                    <AppInputRange
                      getAriaLabel={() => 'Age'}
                      step={1}
                      value={age}
                      valueLabelDisplay="on"
                      onChange={handleChangeAge}
                    />
                  </RangeContainer>
                </Wrapper>
              </InputContainer>
            </section>

            <ButtonGroup>
              <Button text="Cancel" containerStyles={btnStyles} />
              <Button text="Search" type="submit" containerStyles={btnStyles} />
            </ButtonGroup>
          </form>
        )}
      />
    </Container>
  );
};

interface FiltersProps {}

const Container = styled.div`
  padding: 40px;
  background-color: ${color.white};
  border-radius: 6px;
  margin-bottom: 40px;
`;

const titleStyles = css`
  margin-bottom: 16px;
`;

const InputContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;

const Wrapper = styled.div`
  flex-basis: 49%;
  margin-bottom: 24px;
`;

const SelectContainer = styled.div`
  max-height: 96px;
  overflow: auto;
`;

const labelStyles = css`
  display: inline-block;
`;

const RangeContainer = styled.div`
  width: calc(100% - 64px);
  margin: auto;
`;

const tabsStyles = css`
  margin-bottom: 32px;
`;

const ButtonGroup = styled.div`
  display: flex;
`;

const btnStyles = css`
  max-width: 146px;
  &:first-child {
    border: 1px solid ${color.line};
    box-sizing: border-box;
    border-radius: 6px;
    background-color: ${color.white};
    color: ${color.black};
    margin-right: 8px;
  }
`;

export default FiltersForm;
