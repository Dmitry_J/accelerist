import React, { useState } from 'react';

import { CheckBox } from 'src/ui/CheckBox';
import { css } from 'styled-components';

export const IndustrySelect: React.FC<IndustrySelectProps> = ({
  title,
  active,
  handle,
}) => {
  const [isCheck, setIsCheck] = useState<boolean>(false);

  const handleCheck = () => {
    active = !isCheck;
    if (handle) {
      handle(title, active);
    }

    setIsCheck(!isCheck);
  };

  return (
    <CheckBox
      isCheked={isCheck}
      title={title}
      onClick={handleCheck}
      containerStyles={containerStyles}
    />
  );
};

interface IndustrySelectProps {
  title: string;
  active?: boolean;
  handle?: (val: string, active: boolean) => void;
}

const containerStyles = css`
  margin-bottom: 16px;
`;
