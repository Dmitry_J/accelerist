import React from 'react';
import styled from 'styled-components';
import { useHistory } from 'react-router-dom';

import { color } from 'src/styles/color';
import { Title } from 'src/ui/Title';
import ArrowLeft from 'src/assets/images/icons/ArrowLeft';

export const HeaderTitle: React.FC<HeaderTitleProps> = ({
  children,
  title,
  isGoBack,
}) => {
  const history = useHistory();

  const handleGoBack = () => history.goBack();

  return (
    <Container>
      <Wrapper>
        {isGoBack && (
          <button onClick={handleGoBack} style={arrowStyles}>
            <ArrowLeft />
          </button>
        )}
        <Title type="h2" onClick={handleGoBack}>
          {title}
        </Title>
        {children}
      </Wrapper>
    </Container>
  );
};

interface HeaderTitleProps {
  title: string;
  isGoBack?: boolean;
}

const Container = styled.div`
  padding: 24px 0;
  background-color: ${color.white};
`;

const Wrapper = styled.div`
  display: flex;
  align-items: center;
  max-width: 1334px;
  padding: 0 10px;
  margin: auto;
`;

const arrowStyles = {
  margin: 0,
  marginRight: 12,
  padding: 0,
  border: 'none',
  backgroundColor: 'transparent',
  height: 32,
  width: 32,
  cursor: 'pointer',
};
