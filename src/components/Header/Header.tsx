import React, { useState } from 'react';
import styled, { keyframes } from 'styled-components';
import { Link } from 'react-router-dom';
import { fadeIn } from 'react-animations';

import { color } from 'src/styles/color';
import LogoDark from 'src/assets/images/icons/LogoDark';
import { ProfileIcon } from 'src/ui/ProfileIcon';
import { Search } from '../Search';
import { actions } from 'src/store/ducks';
import { useAppDispatch } from 'src/store/hooks';
import { HeaderTitle } from './components/HeaderTitle';
import { routes } from 'src/routes/routes';
import { Title } from 'src/ui/Title';

const Header: React.FC<HeaderProps> = ({ children, title, isGoBack }) => {
  const [isOpenList, setIsOpenList] = useState<boolean>(false);
  const dispatch = useAppDispatch();

  const handleOpenList = () => setIsOpenList(!isOpenList);

  const handleLogOut = () => {
    dispatch(actions.user.logOutAction());
  };

  return (
    <>
      <Container>
        <Wrapper>
          <WrapperContainer>
            <Link to={routes.DASHBOARD} style={LogoWrapper}>
              <LogoDark />
              <Title type="h1" tag="h1">
                Accelerist
              </Title>
            </Link>
          </WrapperContainer>
          <WrapperContainer>
            <Search />
            <ProfileWrapper>
              <ProfileIcon onClick={handleOpenList} />
              {isOpenList && (
                <List>
                  <ListItem onClick={handleLogOut}>Log Out</ListItem>
                </List>
              )}
            </ProfileWrapper>
          </WrapperContainer>
        </Wrapper>
      </Container>
      <HeaderTitle title={title} isGoBack={isGoBack} children={children} />
    </>
  );
};

interface HeaderProps {
  title: string;
  isGoBack?: boolean;
}

const Container = styled.div`
  padding: 19px 0;
  background-color: ${color.headerBgBlue};
`;

const Wrapper = styled.div`
  padding: 0 10px;
  max-width: 1334px;
  margin: auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const WrapperContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const ProfileWrapper = styled.div`
  position: relative;
`;

const List = styled.ul`
  position: absolute;
  padding: 24px;
  min-width: 177px;
  top: calc(100% + 12px);
  left: 0;
  background-color: ${color.white};
  box-shadow: 0px 2px 20px rgba(40, 31, 61, 0.04);
  border-radius: 6px;
  animation: 0.25s ${keyframes`${fadeIn}`};
`;

const ListItem = styled.li`
  font-size: 12px;
  line-height: 150%;
  transition: 0.25s;
  cursor: pointer;
  &:hover {
    color: ${color.red};
  }
`;

const LogoWrapper = {
  marginRight: '50px',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'space-between',
};

export default Header;
