import React from 'react';
import { Link, useRouteMatch } from 'react-router-dom';
import { color } from 'src/styles/color';
import styled from 'styled-components';
import { routes } from 'src/routes/routes';

const TabWrapper: React.FC<TabWrapperProps> = () => {
  const route: RouteParams = useRouteMatch();

  return (
    <>
      <Container>
        <Link
          to={routes.SIGNUP}
          style={route.path === routes.SIGNUP ? linkActiveStyles : linkStyles}
        >
          Register
        </Link>
        <Link
          to={routes.LOGIN}
          style={route.path === routes.LOGIN ? linkActiveStyles : linkStyles}
        >
          Login
        </Link>
      </Container>
    </>
  );
};

interface TabWrapperProps {}

interface RouteParams {
  isExact: boolean;
  params: any; //пустой объект в параметрах
  path: string;
  url: string;
}

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 2px 4px;
  margin-bottom: 32px;
  height: 40px;
  border-radius: 6px;
  background-color: ${color.bgF8};
`;

const linkActiveStyles = {
  width: '50%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: color.secondaryBlue,
  borderRadius: '6px',
  cursor: 'pointer',
  fontSize: '12px',
  lineHeight: '150%',
  color: color.black,
  textDecoration: 'none',
};

const linkStyles = {
  width: '50%',
  height: '100%',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: '6px',
  cursor: 'pointer',
  fontSize: '12px',
  lineHeight: '150%',
  color: color.darkGray,
  textDecoration: 'none',
};

export default TabWrapper;
