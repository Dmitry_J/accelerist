import React from 'react';
import styled, { css } from 'styled-components';

import { color } from 'src/styles/color';
import { AppText } from 'src/ui/AppText';
import { CompanyItemTypes } from 'src/types/types';
import { Link } from 'react-router-dom';
import { routes } from 'src/routes/routes';

export const FavoriteSectionItem: React.FC<FavoriteSectionItemProps> = ({
  item,
}) => {
  return (
    <Container>
      <FavoriteWrapper>
        <Avatar>
          {item.logo ? (
            <img src={item.logo} alt={`Logo company: ${item.name}`} />
          ) : (
            item.name[0].toUpperCase()
          )}
        </Avatar>
        <NameContainer>
          <AppText containerStyles={nameStyles}>
            <Link to={`${routes.COMPANY}/${item.id}`}>{item.name}</Link>
          </AppText>
          <AppText containerStyles={rankStyles}>
            Priority Ranking {item.score}
          </AppText>
        </NameContainer>
      </FavoriteWrapper>
      <div>
        <AppText containerStyles={textStyles}>CSR Focus</AppText>
        <List>
          {item.crsFocus.length > 0 ? (
            item.crsFocus.map((csrItem) => {
              return <ListItem>{csrItem}</ListItem>;
            })
          ) : (
            <div>No information</div>
          )}
        </List>
      </div>
    </Container>
  );
};

interface FavoriteSectionItemProps {
  item: CompanyItemTypes;
}

const Container = styled.div`
  min-height: 156px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 24px;
  margin: 0 12px 15px 12px;
  width: 256px;
  background-color: ${color.white};
  border-radius: 6px;
`;

const FavoriteWrapper = styled.div`
  display: flex;
`;

const Avatar = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-right: 12px;
  height: 48px;
  min-width: 48px;
  border-radius: 6px;
  background-color: ${color.secondaryBlue};
`;

const NameContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const List = styled.ul`
  display: flex;
`;

const ListItem = styled.li`
  position: relative;
  margin-right: 16px;
  font-weight: 500;
  font-size: 12px;
  line-height: 150%;
  &::before {
    content: '';
    position: absolute;
    height: 4px;
    width: 4px;
    top: 50%;
    transform: translateY(-50%);
    right: -10px;
    background-color: ${color.grayC4};
    border-radius: 50%;
  }
  &:last-child {
    margin-right: 0;
    &::before {
      content: none;
    }
  }
`;

const nameStyles = css`
  font-size: 12px;
  line-height: 150%;
  margin: 0;
  font-weight: 500;
`;

const rankStyles = css`
  margin: 0;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;

const textStyles = css`
  margin-bottom: 4px;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
`;
