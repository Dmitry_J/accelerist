import React from 'react';
import styled from 'styled-components';

import { FavoriteSectionItem } from './FavoriteSectionItem';

import { useAppSelector } from 'src/store/hooks';
import { selectors } from 'src/store/ducks';
import { CompanyItemTypes } from 'src/types/types';

export const FavoriteSectionList: React.FC<FavoriteSectionListProps> = () => {
  const favoriteCompanies = useAppSelector(
    selectors.companies.selectItemsCompanies
  );

  return (
    <List>
      {favoriteCompanies.length > 0 &&
        favoriteCompanies.map((item: CompanyItemTypes) => {
          return <FavoriteSectionItem key={item.id} item={item} />;
        })}
    </List>
  );
};

interface FavoriteSectionListProps {}

const List = styled.div`
  margin: 0 -12px;
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
`;
