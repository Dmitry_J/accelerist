import React from 'react';
import styled from 'styled-components';

import { FavoriteSectionList } from './components/FavoriteSectionList';
import { SectionTitle } from 'src/ui/SectionTitle';
import { routes } from 'src/routes/routes';

const FavoritesSection: React.FC<FavoritesSectionProps> = () => {
  return (
    <Container>
      <SectionTitle title="Favorites" link={routes.FAVORITES} />
      <FavoriteSectionList />
    </Container>
  );
};

interface FavoritesSectionProps {}

const Container = styled.div`
  width: 536px;
`;

export default FavoritesSection;
