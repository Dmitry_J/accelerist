import React, { LiHTMLAttributes } from 'react';
import { color } from 'src/styles/color';
import styled, { CSSProp } from 'styled-components';

const TabItem: React.FC<TabItemProps> = ({ title, active, ...props }) => {
  return (
    <ListItem $CSS={title === active ? activeItem : {}} {...props}>
      {title}
    </ListItem>
  );
};

interface TabItemProps extends LiHTMLAttributes<HTMLLIElement> {
  title: string;
  active: string;
}

type RootType = {
  $CSS?: CSSProp;
};

const ListItem = styled.li<RootType>`
  font-weight: normal;
  font-size: 16px;
  line-height: 155%;
  color: ${color.darkGray};
  margin-right: 32px;
  cursor: pointer;
  &:last-child {
    margin-right: 0;
  }
  ${({ $CSS }) => $CSS}
`;

const activeItem = `
  color: ${color.blue}
`;

export default TabItem;
