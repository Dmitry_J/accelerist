import React from 'react';
import { color } from 'src/styles/color';
import styled from 'styled-components';

const TabContent: React.FC<TabContentProps> = ({ content }) => {
  return (
    <ListItem>
      {content.length > 0
        ? content.map((item, i) => <Item key={i}>{item}</Item>)
        : 'No information'}
    </ListItem>
  );
};

interface TabContentProps {
  content: string[];
}

const ListItem = styled.ul`
  display: flex;
  flex-wrap: wrap;
`;

const Item = styled.li`
  padding: 10px;
  background-color: ${color.line};
  border-radius: 6px;
  margin-right: 16px;
  margin-bottom: 16px;
`;

export default TabContent;
