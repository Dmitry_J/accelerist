import React, { useState } from 'react';
import styled from 'styled-components';

import { TabItem } from './components/TabItem';
import { TabContent } from './components/TabContent';
import { breakUpArray } from 'src/utils/breakUpArray';

const tabs = [
  {
    title: 'Similar Companies',
    id: 'Similar Companies',
  },
  {
    title: 'Parent Company',
    id: 'Parent Company',
  },
  {
    title: 'Subsidiaries',
    id: 'Subsidiaries',
  },
];

const TabCompanies: React.FC<TabCompaniesProps> = ({
  similarCompanies,
  parentCompany,
}) => {
  const [activeTab, setActiveTab] = useState<string>(tabs[0].title);
  let activeContent: string[] = [];

  const handleActiveItem = (title: string) => {
    setActiveTab(title);
  };

  const similar = breakUpArray(similarCompanies);

  if (activeTab === 'Similar Companies' && similar) {
    activeContent = similar;
  } else if (activeTab === 'Parent Company' && parentCompany) {
    activeContent = parentCompany;
  } else if (activeTab === 'Subsidiaries') {
    activeContent = [];
  }

  return (
    <Container>
      <List>
        {tabs.map((item) => (
          <TabItem
            key={item.id}
            title={item.title}
            active={activeTab}
            onClick={() => handleActiveItem(item.title)}
          />
        ))}
      </List>
      <TabContent content={activeContent} />
    </Container>
  );
};

interface TabCompaniesProps {
  similarCompanies: string[] | null;
  parentCompany: string[] | null;
}

const Container = styled.div`
  margin-bottom: 22px;
`;

const List = styled.ul`
  display: flex;
  margin-bottom: 24px;
`;

export default TabCompanies;
