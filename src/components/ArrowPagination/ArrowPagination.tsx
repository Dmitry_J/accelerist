import React from 'react';
import styled, { css } from 'styled-components';

import { AppText } from 'src/ui/AppText';
import { getPages } from 'src/utils/formatPages';
import { queryMetaTypes } from 'src/types/types';
import { ArrowButton } from 'src/ui/ArrowButton';

import ArrowLeft from 'src/assets/images/icons/ArrowLeft';
import ArrowRight from 'src/assets/images/icons/ArrowRight';

const ArrowPagination: React.FC<ArrowPaginationProps> = ({
  queryMeta,
  route,
}) => {
  return (
    <Container>
      {queryMeta.currentPage !== '1' && (
        <ArrowButton to={`${route}/${Number(queryMeta.currentPage) - 1}`}>
          <ArrowLeft />
        </ArrowButton>
      )}

      <AppText containerStyles={textStyles}>
        {queryMeta && `${getPages(queryMeta)} of ${queryMeta.totalItems}`}
      </AppText>
      {Number(queryMeta.currentPage) !== queryMeta.totalPages && (
        <ArrowButton to={`${route}/${Number(queryMeta.currentPage) + 1}`}>
          <ArrowRight />
        </ArrowButton>
      )}
    </Container>
  );
};

interface ArrowPaginationProps {
  queryMeta: queryMetaTypes;
  route?: string;
}

const Container = styled.div`
  display: flex;
  align-items: center;
`;

const textStyles = css`
  line-height: 150%;
  margin: 0 12px;
`;

export default ArrowPagination;
