import React, { useEffect } from 'react';
import { Form, Field, FormProps } from 'react-final-form';
import { ToastContainer } from 'react-toastify';

import { useAppDispatch, useAppSelector } from 'src/store/hooks';
import { thunks, selectors, actions } from 'src/store/ducks';
import { AppText } from 'src/ui/AppText';
import { InputField } from '../InputField';
import { validateForm } from 'src/utils/validateFunc';
import { color } from 'src/styles/color';
import { Button } from 'src/ui/Button';
import { errorRequestMessage } from 'src/utils/toastFunctions';
import { css } from 'styled-components';

const SignUpForm: React.FC = () => {
  const dispatch = useAppDispatch();
  const statusRequest = useAppSelector(selectors.user.selectStatus);
  const errorMessage = useAppSelector(selectors.user.selectError);

  const onSubmit = (values: FormProps) => {
    dispatch(
      thunks.user.signUpAction({
        email: values.email,
        password: values.password,
      })
    );
  };

  useEffect(() => {
    if (errorMessage) {
      errorRequestMessage(errorMessage);
      dispatch(actions.user.resetError());
    }
  });

  return (
    <>
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit, submitting, pristine }) => (
          <form onSubmit={handleSubmit}>
            <Field
              name="email"
              validate={validateForm.composeValidators(
                validateForm.required,
                validateForm.correctMail
              )}
              render={({ ...props }) => {
                return (
                  <InputField
                    ladel="Email"
                    placeholderText="Enter email"
                    {...props}
                  />
                );
              }}
            />

            <Field
              name="password"
              validate={validateForm.composeValidators(
                validateForm.required,
                validateForm.minValue()
              )}
              render={({ ...props }) => {
                return (
                  <InputField
                    ladel="Password"
                    placeholderText="Enter password"
                    {...props}
                    containerStyles={inputContainerStyles}
                  />
                );
              }}
            />

            <AppText containerStyles={textStyles}>
              I agree that by clicking{' '}
              <AppText tag="span" containerStyles={spanStyles}>
                “Registration”
              </AppText>{' '}
              I accept the{' '}
              <AppText tag="span" containerStyles={spanStyles}>
                Terms Of Service
              </AppText>{' '}
              and{' '}
              <AppText tag="span" containerStyles={spanStyles}>
                Privacy Policy
              </AppText>
            </AppText>

            <Button
              containerStyles={buttonStyles}
              type="submit"
              text="Submit"
              loading={statusRequest}
              spinColor={color.white}
              disabled={submitting || pristine}
            />
          </form>
        )}
      />
      <ToastContainer
        position="top-right"
        autoClose={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
      />
    </>
  );
};

const textStyles = `
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  text-align: center;
  margin-top: 40px;
  margin-bottom: 16px;
`;

const inputContainerStyles = css`
  margin-bottom: 0;
`;

const buttonStyles = css`
  margin-bottom: 32px;
`;

const spanStyles = css`
  font-family: 'San Francisco Pro';
  font-size: 12px;
  line-height: 150%;
  color: ${color.black};
`;

export default SignUpForm;
