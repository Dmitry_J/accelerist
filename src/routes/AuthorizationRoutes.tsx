import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import * as PersistTypes from 'redux-persist/es/types';

import { RegistrationPage } from 'src/pages/RegistrationPage';
import { LoginPage } from 'src/pages/LoginPage';
import { ResetPage } from 'src/pages/ResetPage';
import { routes } from './routes';

export const AuthorizationRoutes: React.FC<AuthorizationRoutesProps> = ({
  persistor,
}) => {
  return (
    <Router>
      <Switch>
        <Route path={routes.SIGNUP}>
          <RegistrationPage persistor={persistor} />
        </Route>
        <Route path={routes.LOGIN}>
          <LoginPage persistor={persistor} />
        </Route>
        <Route path={routes.RESET}>
          <ResetPage />
        </Route>
        <Redirect to={routes.SIGNUP} />
      </Switch>
    </Router>
  );
};

interface AuthorizationRoutesProps {
  persistor?: PersistTypes.Persistor;
}
