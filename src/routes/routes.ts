const DASHBOARD: string = '/dashboard';
const SIGNUP: string = '/signup';
const LOGIN: string = '/login';
const RESET: string = '/reset';
const PROSPECTS: string = '/prospects';
const FAVORITES: string = '/favorites';
const SUPPORT: string = '/support';
const COMPANY: string = '/company';
const SEARCH: string = '/search';

export const routes = {
  DASHBOARD,
  SIGNUP,
  LOGIN,
  RESET,
  SUPPORT,
  PROSPECTS,
  FAVORITES,
  COMPANY,
  SEARCH,
};
