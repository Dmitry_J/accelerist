import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';

import { DashboardPage } from 'src/pages/DashboardPage';
import { ProspectsPage } from 'src/pages/ProspectsPage';
import { FavoritesPage } from 'src/pages/FavoritesPage';
import { CorparateProfilePage } from 'src/pages/CorparateProfilePage';
import { SearchPage } from 'src/pages/SearchPage';
import { routes } from './routes';

export const RootRoutes: React.FC = () => {
  return (
    <Router>
      <Switch>
        <Route path={routes.DASHBOARD}>
          <DashboardPage />
        </Route>
        <Route path={`${routes.PROSPECTS}/:page?`}>
          <ProspectsPage />
        </Route>
        <Route path={`${routes.FAVORITES}/:page?`}>
          <FavoritesPage />
        </Route>
        <Route path={`${routes.COMPANY}/:id?`}>
          <CorparateProfilePage />
        </Route>
        <Route path={`${routes.SEARCH}/:page?`}>
          <SearchPage />
        </Route>
        <Redirect to={routes.DASHBOARD} />
      </Switch>
    </Router>
  );
};
