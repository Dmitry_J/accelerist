// request types

export enum RequestTypes {
  PENDING = 'pending',
  FULFILLED = 'fulfilled',
  REJECTED = 'rejected',
}

// types

interface queryMetaTypes {
  currentPage: string;
  itemCount: number;
  itemsPerPage: string;
  totalItems: number;
  totalPages: number;
}

interface savedListTypes {
  id: string;
  name: string;
  filters: any;
  prospectsAvailable: number;
  lastAuthor: any;
  createdAt: string;
  updatedAt: string;
}

interface CompanyItemTypes {
  id: string;
  zoomInfoId: null | string;
  name: string;
  logo: null | string;
  ticker: string;
  parentCompany: null | string;
  phone: string;
  fax: string;
  website: string;
  city: string;
  street: string;
  state: string;
  zipCode: null | string;
  country: string;
  continent: null | string;
  productsBrandDescription: null | string;
  descriptionList: string;
  revenueRange: string;
  employeeRange: string;
  twitterHandle: null | string;
  socialMediaUrls: null | string;
  competitors: null | string;
  subUnitIndustries: null | string;
  primaryIndustry: string[];
  industries: null | string;
  revenue: string;
  employeeCount: number;
  annualContributions: null | string;
  cashContributions: null | string;
  inKindContributions: null | string;
  employeeContributions: null | string;
  parentId: null | string;
  parentName: null | string;
  type: null | string;
  sdgGoals: any[];
  genders: null | string;
  income: null | string;
  age: null | string;
  ethnicity: null | string;
  nonprofit: null | string;
  purchase: null | string;
  affiliation: null | string;
  brands: null | string;
  interests: null | string;
  typesOfInvestment: null | string;
  errorLoadZoomInfo: null | string;
  charitablePartners: any[];
  statusZoomInfo: string;
  loadZoomInfoDate: null | string;
  errorLoadZoomInfoDate: null | string;
  partnershipLink: null | string;
  employeeEngagementOpportunities: boolean;
  similarCompanies: string[];
  favoriteCompanies: FavoriteCompaniesTypes[];
  score: number;
  like: boolean;
  crsFocus: any[];
}

interface FavoriteCompaniesTypes {
  id: string;
  companyId: string;
  userId: string;
}

// ApiTypes

interface LogInApiTypes {
  email: string;
  password: string;
}

interface SendMailApiTypes {
  email: string;
}

interface GetSavedList {
  page: number;
  limit: number;
  sort?: string | null;
}

interface GetFavoriteCompanies {
  page: number;
  limit: number;
}

interface GetCompany {
  id: string;
  like?: boolean;
}

export type {
  LogInApiTypes,
  SendMailApiTypes,
  GetSavedList,
  queryMetaTypes,
  savedListTypes,
  CompanyItemTypes,
  GetFavoriteCompanies,
  GetCompany,
};
