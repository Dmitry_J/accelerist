import * as React from 'react';

function CheckOff(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={24}
      height={24}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect x={2.5} y={2.5} width={19} height={19} rx={2.5} stroke="#BFBFBF" />
    </svg>
  );
}

export default CheckOff;
