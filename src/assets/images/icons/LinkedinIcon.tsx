import * as React from 'react';

function LinkedinIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={20}
      height={20}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <g clipPath="url(#prefix__clip0)">
        <path
          d="M10 .063c-5.522 0-10 4.451-10 9.94 0 5.49 4.478 9.942 10 9.942s10-4.452 10-9.941c0-5.49-4.478-9.941-10-9.941zM7.094 15.09H4.66V7.807h2.435v7.284zM5.876 6.812h-.015c-.818 0-1.346-.56-1.346-1.258 0-.715.545-1.259 1.378-1.259.833 0 1.345.544 1.361 1.259 0 .699-.528 1.258-1.378 1.258zm10 8.279H13.44v-3.897c0-.98-.353-1.647-1.234-1.647-.673 0-1.074.45-1.25.885-.064.156-.08.373-.08.591v4.068H8.442s.032-6.6 0-7.284h2.435v1.031c.324-.496.903-1.202 2.195-1.202 1.602 0 2.804 1.04 2.804 3.278v4.177z"
          fill="#0E6597"
        />
      </g>
      <defs>
        <clipPath id="prefix__clip0">
          <path
            fill="#fff"
            transform="translate(0 .063)"
            d="M0 0h20v19.882H0z"
          />
        </clipPath>
      </defs>
    </svg>
  );
}

export default LinkedinIcon;
