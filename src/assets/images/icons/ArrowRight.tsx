import * as React from 'react';

function ArrowRight(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={24}
      height={24}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M7.377 3.377a1.286 1.286 0 011.818 0l7.714 7.714a1.286 1.286 0 010 1.818l-7.714 7.714a1.286 1.286 0 11-1.818-1.818L14.182 12 7.377 5.195a1.286 1.286 0 010-1.818z"
        fill="#000"
      />
    </svg>
  );
}

export default ArrowRight;
