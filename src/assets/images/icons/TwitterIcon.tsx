import * as React from 'react';

function TwitterIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={20}
      height={20}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M10 0C4.478 0 0 4.478 0 10s4.478 10 10 10 10-4.478 10-10S15.522 0 10 0zm4.566 7.797c.004.098.006.197.006.297 0 3.033-2.308 6.53-6.53 6.53a6.497 6.497 0 01-3.518-1.03c.18.02.362.03.547.03a4.605 4.605 0 002.85-.982 2.298 2.298 0 01-2.143-1.594 2.28 2.28 0 001.036-.04 2.296 2.296 0 01-1.84-2.278c.308.172.662.275 1.039.287a2.293 2.293 0 01-.71-3.065 6.517 6.517 0 004.73 2.399 2.296 2.296 0 013.911-2.093 4.604 4.604 0 001.458-.558 2.305 2.305 0 01-1.01 1.27 4.579 4.579 0 001.319-.362c-.307.46-.697.865-1.145 1.189z"
        fill="#2BAEE0"
      />
    </svg>
  );
}

export default TwitterIcon;
