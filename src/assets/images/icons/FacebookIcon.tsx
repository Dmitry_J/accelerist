import * as React from 'react';

function FacebookIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={20}
      height={20}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        d="M10 20c5.523 0 10-4.477 10-10S15.523 0 10 0 0 4.477 0 10s4.477 10 10 10z"
        fill="#3B5998"
      />
      <path
        d="M12.514 10.392H10.73v6.537H8.025v-6.537H6.74V8.094h1.286V6.607c0-1.063.505-2.727 2.728-2.727l2.002.008v2.23h-1.453c-.238 0-.573.119-.573.626v1.352h2.02l-.236 2.296z"
        fill="#fff"
      />
    </svg>
  );
}

export default FacebookIcon;
