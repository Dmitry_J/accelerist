import * as React from 'react';

function CheckOn(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={24}
      height={24}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <rect x={2} y={2} width={20} height={20} rx={3} fill="#CAF0FF" />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M19.166 7.243a.854.854 0 010 1.175l-8.8 9.139a.78.78 0 01-1.131 0l-4-4.154a.854.854 0 010-1.175.78.78 0 011.13 0L9.8 15.794l8.235-8.55a.78.78 0 011.13 0z"
        fill="#122434"
      />
    </svg>
  );
}

export default CheckOn;
