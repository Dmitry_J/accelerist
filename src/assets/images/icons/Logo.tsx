import * as React from 'react';

function Logo(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={36}
      height={36}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path fill="#FFDA00" d="M17.24 10.8h1.013V0H17.24z" />
      <path fill="#F6921E" d="M17.24 36h1.013V25.2H17.24z" />
      <path fill="#E92D30" d="M17.24 17.87l.716.728L30.505 5.87l-.717-.728z" />
      <path
        fill="#E92D30"
        d="M30.505 30.214l-.717.727L17.24 18.213l.717-.728z"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M18.167 18.041l.169-.17L5.787 5.143l-.717.727 11.452 11.616H0v1.028h16.605l-11.534 11.7.717.727 12.549-12.728-.17-.172zM36 18l-5.831-5.657-.546.576 4.855 4.567H27.38v1.028h7.098l-4.855 5.081.546.576L35.999 18z"
        fill="#fff"
      />
    </svg>
  );
}

export default Logo;
