import * as React from 'react';

function FiltersIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      width={24}
      height={24}
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10.667 6.667a.667.667 0 01-.667.666H5.333a.667.667 0 110-1.333H10c.368 0 .667.298.667.667zM18 6.667a.667.667 0 01-.667.666h-4.666a.667.667 0 110-1.333h4.666c.369 0 .667.298.667.667zM12 12a.667.667 0 01-.667.667h-6a.667.667 0 010-1.334h6c.368 0 .667.299.667.667zM18 12a.667.667 0 01-.667.667H14a.667.667 0 010-1.334h3.333c.369 0 .667.299.667.667zM9.333 17.333a.667.667 0 01-.666.667H5.333a.667.667 0 010-1.333h3.334c.368 0 .666.298.666.666zM18 17.333a.667.667 0 01-.667.667h-6a.667.667 0 010-1.333h6c.369 0 .667.298.667.666z"
        fill="#737373"
      />
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M10 4c.368 0 .667.298.667.667v4a.667.667 0 01-1.334 0v-4c0-.369.299-.667.667-.667zM14 9.333c.368 0 .667.299.667.667v4a.667.667 0 11-1.334 0v-4c0-.368.299-.667.667-.667zM8.667 14.667c.368 0 .666.298.666.666v4a.667.667 0 11-1.333 0v-4c0-.368.299-.666.667-.666z"
        fill="#737373"
      />
    </svg>
  );
}

export default FiltersIcon;
