import { createGlobalStyle } from 'styled-components';

import { color } from './color';

export const GlobalStyles = createGlobalStyle`
    html {
        box-sizing: border-box;
        height: 100%;
    }

    *,
    *::before,
    *::after {
        box-sizing: inherit; 
    }

    body {
        margin: 0;
        background-color: ${color.bg};
        font-family: 'Rubik', sans-serif;
        font-weight: normal;
        font-size: 16px;
        color: ${color.black};
        height: 100%;
    }

    p, h1, h2, h3 {
        margin: 0;
        padding: 0;
    }

    #root {
        height: 100%
    }

    a {
        color: inherit;
        text-decoration: none;
    }

    ul {
        margin: 0;
        padding: 0;
        list-style-type: none;
    }

    .disabled-link {
        pointer-events: none;
    }

    input {
        font-family: inherit
    }
`;
