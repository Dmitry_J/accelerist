import React, { useEffect } from 'react';
import styled, { css } from 'styled-components';
import { ToastContainer } from 'react-toastify';
import { useParams } from 'react-router-dom';

import { Header } from 'src/components/Header';
// import { LoadingSpinner } from 'src/ui/LoadingSpinner';
import { CompaniesList } from 'src/components/CompaniesList';
import { useAppSelector, useAppDispatch } from 'src/store/hooks';
import { selectors, thunks } from 'src/store/ducks';
import { ContentContainer } from 'src/layout/ContentContainer';
import { ArrowPagination } from 'src/components/ArrowPagination';
import { routes } from 'src/routes/routes';
// import { RequestTypes } from 'src/types/types';
import { AppText } from 'src/ui/AppText';
// import { errorRequestMessage } from 'src/utils/toastFunctions';

export const FavoritesPage: React.FC = () => {
  const metaFavoritesCompanies = useAppSelector(
    selectors.companies.selectMetaCompanies
  );
  const favoritesCompanies = useAppSelector(
    selectors.companies.selectItemsCompanies
  );
  // const statusRequest = useAppSelector(
  //   selectors.companies.selectRequestCompanies
  // );

  const dispatch = useAppDispatch();
  const routeParams: RouterParams = useParams();

  let page: number = 1;

  if (routeParams.page) page = Number(routeParams.page);

  useEffect(() => {
    dispatch(
      thunks.companies.asyncGetFavoritesCompanies({
        page,
        limit: 15,
      })
    );
  }, [dispatch, page]);

  return (
    <>
      <Header title="favorites" />

      {/* {statusRequest === RequestTypes.PENDING ? (
        <LoadingSpinner />
      ) : (
        <ContentContainer>
          {favoritesCompanies.length === 0 && (
            <Redirect to={`${routes.FAVORITES}`} />
          )}
          <Wrapper>
            <AppText containerStyles={textStyles}>
              {metaFavoritesCompanies.totalItems} companies
            </AppText>
            <ArrowPagination
              queryMeta={metaFavoritesCompanies}
              route={routes.FAVORITES}
            />
          </Wrapper>
          <FavoritesList />
          <ToastContainer
            position="top-right"
            autoClose={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss={false}
            draggable={false}
          />
        </ContentContainer>
      )} */}

      <ContentContainer>
        {/* {favoritesCompanies.length === 0 && (
          <Redirect to={`${routes.FAVORITES}`} />
        )} */}
        <Wrapper>
          <AppText containerStyles={textStyles}>
            {metaFavoritesCompanies.totalItems} companies
          </AppText>
          <ArrowPagination
            queryMeta={metaFavoritesCompanies}
            route={routes.FAVORITES}
          />
        </Wrapper>
        <CompaniesList companies={favoritesCompanies} />
        <ToastContainer
          position="top-right"
          autoClose={false}
          newestOnTop={false}
          closeOnClick
          rtl={false}
          pauseOnFocusLoss={false}
          draggable={false}
        />
      </ContentContainer>
    </>
  );
};
interface RouterParams {
  page: string;
}

const Wrapper = styled.div`
  margin-bottom: 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const textStyles = css`
  font-weight: 500;
  line-height: 145%;
`;
