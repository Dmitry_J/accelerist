import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useParams } from 'react-router-dom';

import { Header } from 'src/components/Header';
import { ContentContainer } from 'src/layout/ContentContainer';
import { SortProspects } from 'src/components/SortProspects';
import { SavedList } from 'src/components/SavedList';
import { ArrowPagination } from 'src/components/ArrowPagination';
import { LoadingSpinner } from 'src/ui/LoadingSpinner';
import { routes } from 'src/routes/routes';
import { RequestTypes } from 'src/types/types';

import { selectors, thunks } from 'src/store/ducks';
import { useAppSelector, useAppDispatch } from 'src/store/hooks';

export const ProspectsPage: React.FC = () => {
  const [sort, setSort] = useState<string | null>(null);
  const dispatch = useAppDispatch();
  const statusRequest = useAppSelector(
    selectors.savedList.selectRequestSavedList
  );
  const metaSavedList = useAppSelector(selectors.savedList.selectMetaSavedList);
  const sortSavedList = useAppSelector(selectors.savedList.selectSortSavedList);
  const routeParams: RouterParams = useParams();

  let page: number = 1;

  if (routeParams.page) page = Number(routeParams.page);

  const handleSort = (sort: string) => {
    if (sort === sortSavedList) {
      setSort(null);
    } else {
      setSort(sort);
    }
  };

  useEffect(() => {
    dispatch(
      thunks.savedList.asyncGetSavedList({
        limit: 15,
        page,
        sort,
      })
    );
  }, [dispatch, page, sort]);

  return (
    <>
      <Header title="prospects" />
      {statusRequest === RequestTypes.PENDING ? (
        <LoadingSpinner />
      ) : (
        <ContentContainer>
          <Wrapper>
            <SortProspects handleSort={handleSort} />
            <ArrowPagination
              queryMeta={metaSavedList}
              route={routes.PROSPECTS}
            />
          </Wrapper>
          <SavedList />
        </ContentContainer>
      )}
    </>
  );
};

interface RouterParams {
  page: string;
}

const Wrapper = styled.div`
  margin-bottom: 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
