import React from 'react';
import styled, { css } from 'styled-components';
import * as PersistTypes from 'redux-persist/es/types';

import { color } from 'src/styles/color';
import { AppText } from 'src/ui/AppText';
import { HeaderEnter } from 'src/ui/HeaderEnter';
import { EnterContainer } from 'src/layout/EnterContainer';
import { EnterFormWrapper } from 'src/layout/EnterFormWrapper';
import { Title } from 'src/ui/Title';
import SocialNetwork from 'src/assets/images/icons/SocialNetwork';
import { SignInForm } from 'src/components/SignInForm';
import { TabWrapper } from 'src/components/TabWrapper';

export const LoginPage: React.FC<LoginPageProps> = ({ persistor }) => {
  return (
    <>
      <HeaderEnter />
      <EnterContainer>
        <EnterFormWrapper>
          <Title>Welcome to Accelerist</Title>

          <TabWrapper />

          <SignInForm persistor={persistor} />
          <AppText containerStyles={textStyles}>or continue with</AppText>

          <IconWrapper>
            <SocialNetwork />
          </IconWrapper>
        </EnterFormWrapper>
      </EnterContainer>
    </>
  );
};

interface LoginPageProps {
  persistor?: PersistTypes.Persistor;
}

const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
`;

const textStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  text-align: center;
  margin-bottom: 16px;
  margin-top: 40px;
  &:last-of-type {
    margin-bottom: 24px;
  }
`;
