import React from 'react';
import * as PersistTypes from 'redux-persist/es/types';
import styled, { css } from 'styled-components';

import { AppText } from 'src/ui/AppText';
import { color } from 'src/styles/color';
import { HeaderEnter } from 'src/ui/HeaderEnter';
import { EnterContainer } from 'src/layout/EnterContainer';
import { EnterFormWrapper } from 'src/layout/EnterFormWrapper';
import { SignUpForm } from 'src/components/SignUpForm';
import { Title } from 'src/ui/Title';
import SocialNetwork from 'src/assets/images/icons/SocialNetwork';
import { TabWrapper } from 'src/components/TabWrapper';

export const RegistrationPage: React.FC<RegistrationPageProps> = ({
  persistor,
}) => {
  // persistor?.pause();
  return (
    <>
      <HeaderEnter />
      <EnterContainer>
        <EnterFormWrapper>
          <Title>Welcome to Accelerist</Title>

          <TabWrapper />
          <SignUpForm />
          <AppText containerStyles={textStyles}>or continue with</AppText>

          <IconWrapper>
            <SocialNetwork />
          </IconWrapper>
        </EnterFormWrapper>
      </EnterContainer>
    </>
  );
};

interface RegistrationPageProps {
  persistor?: PersistTypes.Persistor;
}

const textStyles = css`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  text-align: center;
  margin-bottom: 16px;
  margin-top: 40px;
  &:last-of-type {
    margin-bottom: 24px;
  }
`;

const IconWrapper = styled.div`
  display: flex;
  justify-content: center;
`;
