import React, { useEffect, useState } from 'react';
import styled from 'styled-components';

import { Header } from 'src/components/Header';
import { ContentContainer } from 'src/layout/ContentContainer';
import { SectionTitle } from 'src/ui/SectionTitle';
import { SavedList } from 'src/components/SavedList';
import { FavoritesSection } from 'src/components/FavoritesSection';
import { ReportsSection } from 'src/components/ReportsSection';
import { LoadingSpinner } from 'src/ui/LoadingSpinner';
import { routes } from 'src/routes/routes';
import { RequestTypes } from 'src/types/types';

import { selectors, thunks } from 'src/store/ducks';
import { useAppSelector, useAppDispatch } from 'src/store/hooks';
import { getTeam } from 'src/services/apiFunctions';

export const DashboardPage: React.FC = () => {
  const dispatch = useAppDispatch();
  const statusRequest = useAppSelector(
    selectors.savedList.selectRequestSavedList
  );
  const [searchCount, setSearchCount] = useState<number | null>(null);
  const [pitchCount, setPitchCount] = useState<number | null>(null);

  const asyncGetTeam = async () => {
    try {
      const response = await getTeam();

      setSearchCount(
        (prevState) =>
          prevState !== response.data.searchCount && response.data.searchCount
      );

      setPitchCount(
        (prevState) =>
          prevState !== response.data.pitchCount && response.data.pitchCount
      );
    } catch (e) {
      console.warn(e);
    }
  };

  useEffect(() => {
    dispatch(
      thunks.savedList.asyncGetSavedList({
        limit: 2,
        page: 1,
      })
    );

    dispatch(
      thunks.companies.asyncGetFavoritesCompanies({
        limit: 6,
        page: 1,
      })
    );

    asyncGetTeam();
  }, [dispatch]);

  return (
    <>
      <Header title="dashboard" />
      {statusRequest === RequestTypes.PENDING ? (
        <LoadingSpinner />
      ) : (
        <ContentContainer>
          <SectionTitle title="Prospecting Sessions" link={routes.PROSPECTS} />
          <SavedList />
          <Wrapper>
            <FavoritesSection />
            <ReportsSection searchCount={searchCount} pitchCount={pitchCount} />
          </Wrapper>
        </ContentContainer>
      )}
    </>
  );
};

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;
