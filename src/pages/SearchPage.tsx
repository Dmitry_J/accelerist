import React, { ChangeEvent, useEffect, useState } from 'react';
import styled, { css } from 'styled-components';

import { Header } from 'src/components/Header';
import { ContentContainer } from 'src/layout/ContentContainer';
import { AppText } from 'src/ui/AppText';
import { ArrowPagination } from 'src/components/ArrowPagination';
import { routes } from 'src/routes/routes';
import { LoadingSpinner } from 'src/ui/LoadingSpinner';
import { RequestTypes } from 'src/types/types';
import MailIcon from 'src/assets/images/icons/MailIcon';
import FolderPlus from 'src/assets/images/icons/FolderPlus';
import UploadIcon from 'src/assets/images/icons/UploadIcon';
import { FiltersForm } from 'src/components/FiltersForm';

import { useAppDispatch, useAppSelector } from 'src/store/hooks';
import { selectors, thunks } from 'src/store/ducks';
import { CompaniesList } from 'src/components/CompaniesList';
import { useParams } from 'react-router';
import { color } from 'src/styles/color';
import { Input } from 'src/ui/Input';
import { AppButton } from 'src/ui/AppButton';
import FiltersIcon from 'src/assets/images/icons/FiltersIcon';
import SearchIcon from 'src/assets/images/icons/SearchIcon';

export const SearchPage: React.FC<SearchPageProps> = () => {
  const companies = useAppSelector(selectors.companies.selectItemsCompanies);
  const meta = useAppSelector(selectors.companies.selectMetaCompanies);
  const statusRequest = useAppSelector(
    selectors.companies.selectRequestCompanies
  );
  const [isOpenFilters, setIsOpenFilters] = useState<boolean>(false);
  const [searchCompany, setSearchCompany] = useState<string>('');
  const dispatch = useAppDispatch();

  const routeParams: RouterParams = useParams();

  let page: number = 1;

  if (routeParams.page) page = Number(routeParams.page);

  const handleOpenFilters = () => setIsOpenFilters(!isOpenFilters);

  const handleChangeInput = (e: ChangeEvent<HTMLInputElement>) => {
    setSearchCompany(e.target.value);
  };

  const handleSearchCompany = () => {
    dispatch(
      thunks.companies.asyncGetCompanies({
        searchCompany,
        page: 1,
      })
    );
  };

  useEffect(() => {
    dispatch(
      thunks.companies.asyncGetCompanies({
        searchCompanies: null,
        page,
      })
    );
  }, [dispatch, page]);

  return (
    <>
      <Header title="Search">
        <InputContainer>
          <Input
            containerStyled={inputStyles}
            value={searchCompany}
            onChange={(e) => handleChangeInput(e)}
          />
          <AppButton
            containerStyles={filterIconStyles}
            onClick={handleOpenFilters}
          >
            <FiltersIcon />
          </AppButton>
          <AppButton onClick={handleSearchCompany}>
            <SearchIcon />
          </AppButton>
        </InputContainer>
      </Header>
      {statusRequest === RequestTypes.PENDING ? (
        <LoadingSpinner />
      ) : (
        <ContentContainer>
          {companies.length > 0 ? (
            <>
              {isOpenFilters && <FiltersForm />}
              <AppText containerStyles={textStyles}>
                Found {meta.totalItems} companies
              </AppText>
              <Wrapper>
                <ButtonWrapper>
                  <Button>
                    <FolderPlus style={iconStyles} />
                    Save List
                  </Button>
                  <Button>
                    <UploadIcon style={iconStyles} />
                    Export to Excel
                  </Button>
                  <Button>
                    <MailIcon style={iconStyles} />
                    Accelerist Support
                  </Button>
                </ButtonWrapper>
                <ArrowPagination queryMeta={meta} route={routes.SEARCH} />
              </Wrapper>
              <CompaniesList companies={companies} />
            </>
          ) : (
            <div>No companies found</div>
          )}
        </ContentContainer>
      )}
    </>
  );
};

interface SearchPageProps {}

interface RouterParams {
  page: string;
}

const Wrapper = styled.div`
  margin-bottom: 24px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

const textStyles = css`
  font-weight: 500;
  line-height: 145%;
  margin-bottom: 24px;
`;

const iconStyles = {
  marginRight: 8,
};

const ButtonWrapper = styled.div`
  display: flex;
`;
const Button = styled.button`
  border: none;
  background-color: transparent;
  padding: 0;
  display: flex;
  align-items: center;
  margin-right: 35px;
  cursor: pointer;
  &:last-child {
    margin-right: 0;
  }
`;

const InputContainer = styled.div`
  position: relative;
  width: 100%;
  max-width: 715px;
  height: 36px;
  margin-left: 85px;
`;

const inputStyles = css`
  height: 100%;
  padding: 9px 80px 9px 24px;
  font-family: 'Rubik';
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  background-color: ${color.searchInputGray};
  border: none;
  &:focus {
    border: none;
  }
`;

const filterIconStyles = css`
  right: 50px;
`;
