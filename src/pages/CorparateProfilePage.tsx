import React, { useEffect } from 'react';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';

import { Header } from 'src/components/Header';
import { useAppDispatch, useAppSelector } from 'src/store/hooks';
import { selectors, thunks } from 'src/store/ducks';
import { RequestTypes } from 'src/types/types';
import { LoadingSpinner } from 'src/ui/LoadingSpinner';
import { ContentContainer } from 'src/layout/ContentContainer';
import { ContributionsSection } from 'src/ui/ContributionsSection';

import { NewsSection } from 'src/components/NewsSection';
import { DescriptionSection } from 'src/components/DescriptionSection';
import { TitleCompanySection } from 'src/components/TitleCompanySection';
import { TabCompanies } from 'src/components/TabCompanies';
import { TickerSection } from 'src/ui/TickerSection';
import { ContactsSection } from 'src/ui/ContactsSection';
import { ReportedSection } from 'src/ui/ReportedSection';
import { GoalSection } from 'src/ui/GoalSection';
import { InvestmentSection } from 'src/ui/InvestmentSection';

import { color } from 'src/styles/color';
import { Title } from 'src/ui/Title';
import { CharitablePartners } from 'src/ui/CharitablePartners';

export const CorparateProfilePage: React.FC<CorparateProfilePageProps> = () => {
  const company = useAppSelector(selectors.companies.selectCompany);
  const stetusRequest = useAppSelector(
    selectors.companies.selectRequestCompanies
  );
  const params: ParamsType = useParams();
  const dispatch = useAppDispatch();

  useEffect(() => {
    dispatch(
      thunks.companies.asyncGetOneCompany({
        id: params.id,
      })
    );
  }, [dispatch, params]);

  return (
    <>
      <Header title="Corparate Profile" isGoBack={true} />
      {stetusRequest === RequestTypes.PENDING ? (
        <LoadingSpinner />
      ) : (
        <ContentContainer>
          <TitleCompanySection
            name={company.name}
            like={company.like}
            logo={company.logo}
            primaryIndustry={company.primaryIndustry}
          />
          <Section>
            <MainSection>
              <Title type="h3" tag="h3" containerStyles={titleStyles}>
                Business Description Products
              </Title>
              <DescriptionSection description={company.descriptionList} />
              <TabCompanies
                similarCompanies={company.similarCompanies}
                parentCompany={company.parentCompany}
              />
              <ReportedSection
                employeeCount={company.employeeCount}
                revenue={company.revenue}
              />
              <TickerSection ticker={company.ticker} />
              <ContactsSection
                country={company.country}
                phone={company.phone}
                street={company.street}
                website={company.website}
              />
              <Title type="h3" tag="h3" containerStyles={titleStyles}>
                Social Impact
              </Title>
              <InvestmentSection />
              <GoalSection sdgGoals={company.sdgGoals} />
              <ContributionsSection
                cashContributions={company.cashContributions}
                employeeContributions={company.employeeContributions}
                inKindContributions={company.inKindContributions}
              />
              <CharitablePartners
                charitablePartners={company.charitablePartners}
              />
              <Title type="h4" tag="h4">
                Partnership and Program Details
              </Title>
              <WebsiteLink href={`https://${company.website}`}>
                Go to the company's website
              </WebsiteLink>
            </MainSection>

            <NewsSection />
          </Section>
        </ContentContainer>
      )}
    </>
  );
};

interface CorparateProfilePageProps {}

interface ParamsType {
  id: string;
}

const titleStyles = `margin-bottom: 24px;`;

const Section = styled.div`
  display: flex;
`;

const MainSection = styled.div`
  flex-grow: 1;
  padding: 32px 40px;
  background-color: ${color.white};
`;

const WebsiteLink = styled.a`
  font-size: 16px;
  line-height: 155%;
  text-decoration-line: underline;
  color: ${color.blue};
`;
