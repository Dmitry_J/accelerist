import React, { useState, useEffect } from 'react';
import { Form, Field, FormProps } from 'react-final-form';
import { Link } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import styled, { css } from 'styled-components';

import { Title } from 'src/ui/Title';
import { AppText } from 'src/ui/AppText';
import { EnterContainer } from 'src/layout/EnterContainer';
import { EnterFormWrapper } from 'src/layout/EnterFormWrapper';
import { HeaderEnter } from 'src/ui/HeaderEnter';
import { InputField } from 'src/components/InputField';
import { Button } from 'src/ui/Button';
import { validateForm } from 'src/utils/validateFunc';
import { color } from 'src/styles/color';
import { sendMailChangePassword } from 'src/services/apiFunctions';
import { RequestTypes } from 'src/types/types';
import { routes } from 'src/routes/routes';

import { formatTime } from 'src/utils/formatTime';
import { errorRequestMessage } from 'src/utils/toastFunctions';

export const ResetPage: React.FC = () => {
  const [isSubmit, setIsSubmit] = useState<boolean>(false);
  const [loading, setLoading] = useState<string>('');
  const [time, setTime] = useState<number>(60);

  const onSubmit = (values: FormProps) => {
    setLoading(RequestTypes.PENDING);
    sendMailChangePassword({
      email: values.email,
    })
      .then((res) => {
        console.log(res);
        setIsSubmit(true);
        setTime(60);
        setLoading(RequestTypes.FULFILLED);
      })
      .catch((e) => {
        console.log(e);
        errorRequestMessage(e);
        setIsSubmit(true);
        setTime(59);
        setLoading(RequestTypes.REJECTED);
      });
  };

  useEffect(() => {
    if (isSubmit && time > 0) {
      setTimeout(() => {
        setTime(time - 1);
      }, 1000);
    }
  });

  return (
    <>
      <HeaderEnter />
      <EnterContainer>
        <Container>
          <EnterFormWrapper>
            <Form
              onSubmit={onSubmit}
              render={({ handleSubmit, submitting, pristine }) => (
                <form onSubmit={handleSubmit}>
                  <Title containerStyles={titleStyles}>Password Reset</Title>
                  {!isSubmit ? (
                    <AppText containerStyles={textStyles}>
                      Enter your email to receive instructions on how to reset
                      your password.
                    </AppText>
                  ) : (
                    <AppText containerStyles={textStyles}>
                      A link was sent to your email to confirm password reset
                      and create a new one
                    </AppText>
                  )}
                  {!isSubmit && (
                    <Field
                      name="email"
                      validate={validateForm.composeValidators(
                        validateForm.required,
                        validateForm.correctMail
                      )}
                      render={({ ...props }) => {
                        return (
                          <InputField
                            ladel="Email"
                            placeholderText="Enter email"
                            {...props}
                            containerStyles={inputContainerStyles}
                          />
                        );
                      }}
                    />
                  )}

                  {!isSubmit ? (
                    <Button
                      type="submit"
                      text="Reset"
                      disabled={submitting || pristine}
                      loading={loading}
                      spinColor={color.white}
                    />
                  ) : (
                    <Button
                      type="submit"
                      text={time !== 0 ? formatTime(time) : 'Resend'}
                      disabled={time > 0}
                      loading={loading}
                      spinColor={color.white}
                      containerStyles={time !== 0 ? buttonStyles : {}}
                    />
                  )}
                </form>
              )}
            />
          </EnterFormWrapper>
          {isSubmit && (
            <Link to={routes.SUPPORT} style={linkStyles}>
              Contact Support
            </Link>
          )}
        </Container>
        <Link to={routes.LOGIN} style={linkStyles}>
          Return to login
        </Link>
      </EnterContainer>
      <ToastContainer
        position="top-right"
        autoClose={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
      />
    </>
  );
};

const titleStyles = css`
  text-align: start;
`;

const textStyles = css`
  margin-bottom: 32px;
`;

const inputContainerStyles = css`
  margin-bottom: 40px;
`;

const linkStyles = {
  padding: '9px 24px',
  backgroundColor: color.opacityBlack,
  borderRadius: '6px',
  fontFamily: 'Rubik',
  fontWeight: 500,
  fontSize: '12px',
  lineHeight: '150%',
  color: color.white,
  marginBottom: '28px',
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const buttonStyles = css`
  font-family: 'Rubik';
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
`;
