export const formatTime = (num: number) => {
  let a: string = Math.floor(num / 60).toString();
  let b: string = (num % 60).toString();
  if (a.toString().length === 1) a = `0${a}`;
  if (b.toString().length === 1) b = `0${b}`;
  return `${a}:${b}`;
};

export const formatDate = (isoFormatDate: string) => {
  const lastActivity = new Date(isoFormatDate);
  let month: string = '';

  switch (lastActivity.getMonth()) {
    case 0:
      month = 'Jan';
      break;
    case 1:
      month = 'Feb';
      break;
    case 2:
      month = 'Mar';
      break;
    case 3:
      month = 'Apr';
      break;
    case 4:
      month = 'May';
      break;
    case 5:
      month = 'June';
      break;
    case 6:
      month = 'July';
      break;
    case 7:
      month = 'Aug';
      break;
    case 8:
      month = 'Sep';
      break;
    case 9:
      month = 'Oct';
      break;
    case 10:
      month = 'Nov';
      break;
    case 11:
      month = 'Dec';
      break;
    default:
      break;
  }
  return `${lastActivity.getDate()} ${month} ${lastActivity.getFullYear()}`;
};
