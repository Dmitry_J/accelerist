import { toast } from 'react-toastify';

export const errorRequestMessage = (e: any | unknown) =>
  toast.error(`Error! ${e}`, {
    position: 'top-right',
    autoClose: false,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: true,
    draggable: true,
    progress: undefined,
  });

export const successRequestMessage = (message: string) => {
  toast.success(`Well done! ${message}`, {
    position: 'top-right',
    autoClose: false,
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: false,
    progress: undefined,
  });
};
