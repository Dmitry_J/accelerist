import { queryMetaTypes } from 'src/types/types';

export const getPages = (item: queryMetaTypes) => {
  if (item) {
    const currentPage = item.currentPage;
    const totalItems = item.totalItems;
    const itemsPerPage = item.itemsPerPage;
    if (currentPage === '1' && totalItems > Number(itemsPerPage)) {
      return `${currentPage}-${itemsPerPage}`;
    } else {
      const a = Number(itemsPerPage) * Number(currentPage);
      const b = a - Number(itemsPerPage) + 1;
      if (a > totalItems) {
        return `${b}-${totalItems}`;
      }
      return `${b}-${a}`;
    }
  }
  return '';
};
