export const industry = [
  {
    title: 'Agriculture',
    active: false,
  },
  {
    title: 'Entertainment and Recrea..',
    active: false,
  },
  {
    title: 'Bars and Food Sevices',
    active: false,
  },
  {
    title: 'Test',
    active: false,
  },
];

export const geographicLocation = [
  'Alabama',
  'Alaska',
  'American Samoa',
  'Test',
];

export const sdgGoals = [
  {
    title: 'No Poverty',
    active: false,
  },
  {
    title: 'Zero Hunger',
    active: false,
  },
  {
    title: 'Good Health and well-being',
    active: false,
  },
  {
    title: 'Quality Education',
    active: false,
  },
  {
    title: 'Gender Equality',
    active: false,
  },
  {
    title: 'Clean Water and Sanitation',
    active: false,
  },
  {
    title: 'Affordable and Clean Energy',
    active: false,
  },
  {
    title: 'Decent Work and Economic Growth',
    active: false,
  },
  {
    title: 'Industry',
    active: false,
  },
  {
    title: 'Innovation and Infrastructure',
    active: false,
  },
  {
    title: 'Reduced Inequalities',
    active: false,
  },
  {
    title: 'Sustainable Cities and Communities',
    active: false,
  },
  {
    title: 'Responsible Consumption and Production',
    active: false,
  },
  {
    title: 'Climate Action',
    active: false,
  },
  {
    title: 'Life Below Water',
    active: false,
  },
  {
    title: 'Life On Land, Peace',
    active: false,
  },
  {
    title: 'Justice and Strong Institutions',
    active: false,
  },
  {
    title: 'Partnerships for the Goals',
    active: false,
  },
];

export const cdrFocus = [
  {
    title: 'Human Services',
    active: false,
  },
  {
    title: 'Food Services',
    active: false,
  },
  {
    title: 'Sports',
    active: false,
  },
  {
    title: 'Entertainment',
    active: false,
  },
  {
    title: 'Medical',
    active: false,
  },
  {
    title: 'Business',
    active: false,
  },
];

export const gender = ['male', 'female', 'both'];

export const relations = ['Single', 'Married'];

export const householdIncome = [
  {
    title: 'Less than $20K',
    active: false,
  },
  {
    title: '$20K-$29K',
    active: false,
  },
  {
    title: '$30K-$39K',
    active: false,
  },
  {
    title: '$40K-$49K',
    active: false,
  },
  {
    title: '$50K-$74K',
    active: false,
  },
  {
    title: '$75K-$99K',
    active: false,
  },
  {
    title: '$100K-$124K',
    active: false,
  },
  {
    title: '$125K or More',
    active: false,
  },
];

export const ethnicity = [
  {
    title: 'American',
    active: false,
  },
  {
    title: 'Test',
    active: false,
  },
];
