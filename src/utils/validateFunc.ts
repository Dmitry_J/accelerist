const required = (value: string) => (value ? undefined : 'Required');

const minValue =
  (min: number = 6) =>
  (value: string) =>
    value.length >= min
      ? undefined
      : `Minimum password length is ${min} symbols`;

const correctMail = (value: string) => {
  const reg = /^([A-Za-z0-9_\-.])+@([A-Za-z0-9_\-.])+\.([A-Za-z]{2,4})$/;
  if (!reg.test(value)) {
    return 'Incorrect email format';
  } else {
    return undefined;
  }
};

const composeValidators =
  (...validators: Array<(value: string) => undefined | any>) =>
  (value: string) =>
    validators.reduce(
      (error, validator) => error || validator(value),
      undefined
    );

export const validateForm = {
  composeValidators,
  required,
  minValue,
  correctMail,
};
