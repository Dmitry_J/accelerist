export const sort: SortTypes[] = [
  {
    title: 'Alphabet',
    sortName: 'alphabet',
  },
  {
    title: 'Prospects Available',
    sortName: 'available',
  },
  {
    title: 'Last Activity',
    sortName: 'last-activity',
  },
];

export interface SortTypes {
  title: string;
  sortName: string;
}
