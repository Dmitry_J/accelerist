export const breakUpArray = (arr: string[] | null) => {
  if (!arr || arr.length === 0 || arr[0] === null) {
    return;
  } else {
    return arr[0].split(', ');
  }
};
