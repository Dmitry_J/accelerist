export const formatWord = (word: string | number | null) => {
  if (!word) {
    return word;
  } else {
    let format = word.toString().replace(/(.)(?=(\d{3})+$)/g, '$1,');
    return format;
  }
};

export const formatPhone = (phone: string | number) => {
  let format = phone.toString().replace(/[^+\d]/g, '');
  return format;
};

export const valueText = (value: number) => {
  return `$ ${value}M`;
};

export const trimString = (value: string) => {
  if (value.length > 50) {
    return value.substring(0, 50) + '...';
  } else {
    return value;
  }
};
