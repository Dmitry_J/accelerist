import React, { ComponentType, HTMLAttributes } from 'react';
import { color } from 'src/styles/color';
import styled, { CSSProp, css } from 'styled-components';

export const Title = ({
  children,
  tag = 'h2',
  type,
  containerStyles = {},
  ...props
}: TitleProps): JSX.Element => {
  return (
    <Root type={type} as={tag} $CSS={containerStyles} {...props}>
      {children}
    </Root>
  );
};

interface TitleProps extends HTMLAttributes<HTMLElement> {
  containerStyles?: CSSProp;
  type?: string;
  tag?: string | ComponentType<any> | undefined;
}

type RootType = {
  $CSS: CSSProp;
  type?: string;
};

const Root = styled.h2<RootType>`
  margin-top: 0;
  margin-bottom: 25px;
  font-style: normal;
  font-weight: 500;
  font-size: 24px;
  line-height: 148%;
  text-align: center;
  ${({ type }) => {
    if (type === 'h1') {
      return typeH1;
    } else if (type === 'h2') {
      return typeH2;
    } else if (type === 'h3') {
      return typeH3;
    } else {
      return typeH4;
    }
  }}
  ${({ $CSS }) => $CSS}
`;

const typeH1 = css`
  margin-left: 16px;
  margin-bottom: 0;
  font-family: 'Gotham Rounded';
  font-style: normal;
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
  letter-spacing: 0.2em;
  color: ${color.black};
  text-transform: uppercase;
`;

const typeH2 = css`
  margin: 0;
  font-weight: 500;
  font-size: 32px;
  line-height: 150%;
  text-align: start;
  text-transform: capitalize;
`;

const typeH3 = css`
  margin-bottom: 0;
  font-weight: 500;
  font-size: 24px;
  line-height: 148%;
  text-align: start;
`;

const typeH4 = css`
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
  text-align: start;
  margin-bottom: 16px;
`;

/* 
h2
font-weight: 500;
font-size: 32px;
line-height: 150%;
color: #122434;


h3
font-weight: 500;
font-size: 24px;
line-height: 148%;


h4
font-weight: 500;
font-size: 16px;
line-height: 145%;


*/
