import React from 'react';
import { color } from 'src/styles/color';
import styled from 'styled-components';

import { formatPhone } from 'src/utils/formatWord';
import Globe from 'src/assets/images/icons/Globe';
import Phone from 'src/assets/images/icons/Phone';
import MapPin from 'src/assets/images/icons/MapPin';
import { Title } from './Title';

export const ContactsSection: React.FC<ContactsSectionProps> = ({
  street,
  phone,
  website,
  country,
}) => {
  return (
    <Container>
      <Title type="h4" tag="h4">
        Company Contacts
      </Title>
      <ContactsWrapper>
        <ContactsBlock>
          <ContactsLinkWrapper>
            <Globe />
            <ContactsLink href={`https://${website}`}>{website}</ContactsLink>
          </ContactsLinkWrapper>
          <ContactsLinkWrapper>
            <Phone />
            <ContactsLink href={`tel:${formatPhone(phone)}`}>
              {phone}
            </ContactsLink>
          </ContactsLinkWrapper>
        </ContactsBlock>
        <ContactBlockBottom>
          <MapPin />
          <ContactsStreet>
            {street}, {country}
          </ContactsStreet>
        </ContactBlockBottom>
      </ContactsWrapper>
    </Container>
  );
};

interface ContactsSectionProps {
  street: string;
  country: string;
  phone: string;
  website: string;
}

const Container = styled.div`
  margin-bottom: 60px;
`;

const ContactsWrapper = styled.div`
  border: 1px solid ${color.line};
  border-radius: 6px;
  padding: 20px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const ContactsBlock = styled.div`
  display: flex;
  margin-bottom: 14px;
`;

const ContactsLinkWrapper = styled.div`
  display: flex;
  align-items: center;
  &:first-child {
    margin-right: 25px;
  }
`;

const ContactsLink = styled.a`
  font-weight: normal;
  font-size: 12px;
  line-height: 150%;
  margin-left: 10px;
`;

const ContactBlockBottom = styled.div`
  display: flex;
  align-items: center;
`;

const ContactsStreet = styled.span`
  margin-left: 10px;
  font-weight: normal;
  font-size: 12px;
  line-height: 150%;
`;
