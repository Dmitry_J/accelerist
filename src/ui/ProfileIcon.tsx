import React, { HTMLAttributes } from 'react';
import styled, { css } from 'styled-components';

import Profile from 'src/assets/images/icons/Profile';
import { AppText } from './AppText';
import { color } from 'src/styles/color';

export const ProfileIcon: React.FC<ProfileIconProps> = ({
  avatar,
  ...props
}) => {
  return (
    <Root {...props}>
      <IconWrapper>{!avatar && <Profile />}</IconWrapper>
      <AppText containerStyles={textStyles}>No name</AppText>
    </Root>
  );
};

interface ProfileIconProps extends HTMLAttributes<HTMLDivElement> {
  avatar?: string;
}

const Root = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`;

const IconWrapper = styled.div`
  margin-right: 12px;
  width: 36px;
  height: 36px;
  display: flex;
  align-items: center;
  justify-content: center;
  border-radius: 6px;
  background-color: ${color.white};
`;

const textStyles = css`
  font-size: 12px;
  line-height: 150%;
`;
