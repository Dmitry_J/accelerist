import React from 'react';
import { Link, LinkProps } from 'react-router-dom';
import styled from 'styled-components';

export const ArrowButton = ({ children, ...props }: LinkProps): JSX.Element => {
  return <Root {...props}>{children}</Root>;
};

const Root = styled(Link)`
  display: flex;
  background-color: transparent;
  border: none;
  padding: 0;
  cursor: pointer;
  &:disabled {
    cursor: not-allowed;
  }
`;
