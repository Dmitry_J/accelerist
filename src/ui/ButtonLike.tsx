import React, { ButtonHTMLAttributes } from 'react';
import styled, { CSSProp } from 'styled-components';
import ReactLoading from 'react-loading';

import { color } from 'src/styles/color';
import { RequestTypes } from 'src/types/types';
import LikeOn from 'src/assets/images/icons/LikeOn';
import LikeOff from 'src/assets/images/icons/LikeOff';

export const ButtonLike = ({
  like,
  containerStyles = {},
  loading,
  spinColor = color.blue,
  ...props
}: ButtonLikeProps): JSX.Element => {
  return (
    <Root $CSS={containerStyles} {...props}>
      {loading === RequestTypes.PENDING ? (
        <ReactLoading type="spin" color={spinColor} height={20} width={20} />
      ) : like ? (
        <LikeOn />
      ) : (
        <LikeOff />
      )}
    </Root>
  );
};

interface ButtonLikeProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  like: boolean;
  containerStyles?: CSSProp;
  loading?: string;
  spinColor?: string;
}

type ButtonRoot = {
  $CSS?: CSSProp;
};

const Root = styled.button<ButtonRoot>`
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 36px;
  border: none;
  min-height: 36px;
  background-color: ${color.white};
  border-radius: 6px;
  transition: 0.25s;
  cursor: pointer;
  border: 1px solid ${color.line};
  &:hover {
    border: 1px solid ${color.red};
  }
  &:disabled {
    background-color: ${color.secondaryBlue};
    color: ${color.blue};
    cursor: not-allowed;
  }
  ${({ $CSS }) => $CSS};
`;
