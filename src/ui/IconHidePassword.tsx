import React, { HTMLAttributes } from 'react';
import styled from 'styled-components';

import EyeOff from 'src/assets/images/icons/EyeOff';
import EyeOn from 'src/assets/images/icons/EyeOn';

export const IconHidePassword = ({
  isHide,
  ...props
}: IconHidePasswordProps): JSX.Element => {
  return <Root {...props}>{isHide ? <EyeOff /> : <EyeOn />}</Root>;
};

interface IconHidePasswordProps extends HTMLAttributes<HTMLDivElement> {
  isHide: boolean;
}

const Root = styled.div`
  position: absolute;
  top: 50%;
  right: 16px;
  width: 24px;
  height: 24px;
  transform: translateY(-50%);
  cursor: pointer;
`;
