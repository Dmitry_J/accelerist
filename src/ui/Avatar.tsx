import React, { HTMLAttributes } from 'react';
import { color } from 'src/styles/color';
import styled, { CSSProp } from 'styled-components';

export const Avatar = ({
  containerStyles = {},
  avatar = '',
  name,
  ...props
}: AvatarProps): JSX.Element => {
  return (
    <Root $CSS={containerStyles} {...props}>
      {!avatar ? name : <img src={avatar} alt={name} />}
    </Root>
  );
};

interface AvatarProps extends HTMLAttributes<HTMLDivElement> {
  containerStyles?: CSSProp;
  avatar?: string;
  name: string;
}

type RootType = {
  $CSS?: CSSProp;
  src?: string;
};

const Root = styled.div<RootType>`
  margin-right: 10px;
  width: 32px;
  height: 32px;
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  background-color: ${color.secondaryBlue};
  background-image: url(${({ src }) => src});
  ${({ $CSS }) => $CSS}
`;
