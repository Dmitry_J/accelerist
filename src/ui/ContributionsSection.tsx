import React from 'react';
import { color } from 'src/styles/color';
import { formatWord } from 'src/utils/formatWord';
import styled from 'styled-components';

export const ContributionsSection: React.FC<ContributionsSectionProps> = ({
  cashContributions,
  employeeContributions,
  inKindContributions,
}) => {
  return (
    <ContributionsContainer>
      <TitleBlock>Contributions</TitleBlock>
      <ContributionsTable>
        <ContributionsBlock>
          <ContributionsCell>
            <ContributionsText>Cash Contributions</ContributionsText>
            <ContributionsCount>
              {cashContributions ? formatWord(cashContributions) : '-'}
            </ContributionsCount>
          </ContributionsCell>
          <ContributionsCell>
            <ContributionsText>Employee Contributions</ContributionsText>
            <ContributionsCount>
              {employeeContributions ? formatWord(employeeContributions) : '-'}
            </ContributionsCount>
          </ContributionsCell>
        </ContributionsBlock>
        <ContributionsBlock>
          <ContributionsCell>
            <ContributionsText>Total Social Contributions</ContributionsText>
            <ContributionsCount>
              {inKindContributions ? formatWord(inKindContributions) : '-'}
            </ContributionsCount>
          </ContributionsCell>
          <ContributionsCell>
            <ContributionsText>In-Kind Contributions</ContributionsText>
            <ContributionsCount>
              {inKindContributions ? formatWord(inKindContributions) : '-'}
            </ContributionsCount>
          </ContributionsCell>
        </ContributionsBlock>
      </ContributionsTable>
    </ContributionsContainer>
  );
};

interface ContributionsSectionProps {
  inKindContributions: number | null;
  employeeContributions: number | null;
  cashContributions: number | null;
}

const TitleBlock = styled.h3`
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
  margin-bottom: 16px;
`;

const ContributionsContainer = styled.div`
  margin-bottom: 32px;
`;

const ContributionsTable = styled.div`
  border: 1px solid ${color.line};
  border-radius: 6px;
`;

const ContributionsBlock = styled.div`
  display: flex;
  &:first-child {
    border-bottom: 1px solid ${color.line};
  }
`;

const ContributionsCell = styled.div`
  flex-basis: 50%;
  padding: 16px;
  display: flex;
  flex-direction: column;
  align-items: center;
  &:first-child {
    border-right: 1px solid ${color.line};
  }
`;

const ContributionsText = styled.span`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 4px;
`;

const ContributionsCount = styled.span`
  font-weight: 500;
  line-height: 145%;
`;
