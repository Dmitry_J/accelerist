import React, { ComponentType, HTMLAttributes } from 'react';
import styled, { css, CSSProp } from 'styled-components';

import { color } from 'src/styles/color';

export const AppText = ({
  tag = 'p',
  containerStyles = {},
  children,
  ...props
}: TextProps): JSX.Element => {
  return (
    <Root as={tag} $CSS={containerStyles} {...props}>
      {children}
    </Root>
  );
};

interface TextProps extends HTMLAttributes<HTMLElement> {
  containerStyles?: CSSProp;
  type?: string;
  tag?: string | ComponentType<any> | undefined;
}

type RootType = {
  $CSS: CSSProp;
  type?: string;
};

const Root = styled.p<RootType>`
  font-size: 16px;
  line-height: 155%;
  color: ${color.black};
  ${({ type }) => {
    if (type === 'label') {
      return typeLabel;
    }
  }}
  ${({ $CSS }) => $CSS}
`;

const typeLabel = css`
  font-family: 'Rubik';
  font-style: normal;
  font-weight: normal;
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 4px;
`;
