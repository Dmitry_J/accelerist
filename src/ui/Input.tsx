import React, { InputHTMLAttributes } from 'react';
import styled, { CSSProp } from 'styled-components';

import { color } from 'src/styles/color';

export const Input = ({
  containerStyled = {},
  ...props
}: InputProps): JSX.Element => {
  return <Root $CSS={containerStyled} {...props} />;
};

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  containerStyled?: CSSProp;
}

type ButtonRoot = {
  $CSS?: CSSProp;
};

const Root = styled.input<ButtonRoot>`
  padding: 10px 16px;
  height: 46px;
  width: 100%;
  border: 1px solid ${color.line};
  border-radius: 6px;
  background-color: ${color.white};
  outline: none;
  transition: 0.25s;
  font-size: 16px;
  line-height: 155%;
  &:focus {
    border: 1px solid ${color.blue};
  }
  ${({ $CSS }) => $CSS};
`;
