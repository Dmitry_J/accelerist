import React from 'react';
import styled from 'styled-components';
import { Title } from './Title';

export const TickerSection: React.FC<TickerSectionProps> = ({ ticker }) => {
  return (
    <Container>
      <Title type="h4" tag="h4">
        Company Ticker
      </Title>
      {ticker ? (
        <TickerWrapper>
          <TickerBlock>
            <TickerTitle>WMT</TickerTitle>
            <TickerText>London Stock Exchange</TickerText>
          </TickerBlock>
          <TickerBlock>
            <TickerTitle>WMR</TickerTitle>
            <TickerText>Nasdaq</TickerText>
          </TickerBlock>
          <TickerBlock>
            <TickerTitle>WLMT</TickerTitle>
            <TickerText>Stock Exchange of Singapore</TickerText>
          </TickerBlock>
        </TickerWrapper>
      ) : (
        <TickerBlock>No tickers</TickerBlock>
      )}
    </Container>
  );
};

interface TickerSectionProps {
  ticker?: string[];
}

const Container = styled.div`
  margin-bottom: 32px;
`;

const TickerWrapper = styled.div`
  display: flex;
  justify-content: space-between;
`;

const TickerBlock = styled.div`
  padding: 24px 16px 24px 24px;
  display: flex;
  flex-direction: column;
  border: 1px solid #e8e8e8;
  box-sizing: border-box;
  border-radius: 6px;
  width: 206px;
`;

const TickerTitle = styled.span`
  font-weight: 500;
  font-size: 24px;
  line-height: 148%;
  color: #122434;
  margin-bottom: 8px;
`;

const TickerText = styled.span`
  font-size: 12px;
  line-height: 150%;
  color: #737373;
`;
