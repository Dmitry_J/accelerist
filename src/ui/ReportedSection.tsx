import React from 'react';
import styled from 'styled-components';

import { color } from 'src/styles/color';
import { formatWord } from 'src/utils/formatWord';
import { Title } from './Title';

export const ReportedSection: React.FC<ReportedSectionProps> = ({
  employeeCount,
  revenue,
}) => {
  return (
    <Container>
      <Title type="h4" tag="h4">
        Reported
      </Title>
      <ReportedWrapper>
        <ReportedBlock>
          <ReportedText>Revenue Reported</ReportedText>
          <ReportedCount>
            {revenue ? `$ ${formatWord(revenue)}` : '-'}
          </ReportedCount>
        </ReportedBlock>
        <ReportedBlock>
          <ReportedText>Employees Reported</ReportedText>
          <ReportedCount>
            {employeeCount ? `${formatWord(employeeCount)}` : '-'}
          </ReportedCount>
        </ReportedBlock>
      </ReportedWrapper>
    </Container>
  );
};

interface ReportedSectionProps {
  employeeCount: number | null;
  revenue: string | null;
}

const Container = styled.div`
  margin-bottom: 32px;
`;

const ReportedWrapper = styled.div`
  border: 1px solid ${color.line};
  border-radius: 6px;
  display: flex;
`;

const ReportedBlock = styled.div`
  display: flex;
  flex-basis: 50%;
  flex-direction: column;
  align-items: center;
  padding: 16px;
  &:first-child {
    border-right: 1px solid ${color.line};
  }
`;

const ReportedText = styled.span`
  font-size: 12px;
  line-height: 150%;
  color: ${color.darkGray};
  margin-bottom: 4px;
`;

const ReportedCount = styled.span`
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
`;
