import React from 'react';
import { color } from 'src/styles/color';
import styled from 'styled-components';
import { Title } from './Title';

export const GoalSection: React.FC<GoalSectionProps> = ({ sdgGoals }) => {
  return (
    <AligmentSection>
      <Title type="h4" tag="h4">
        SDG Goal Alignment
      </Title>
      {sdgGoals.length > 0 ? (
        sdgGoals.map((item: string) => (
          <AligmentBlock key={item}>{item}</AligmentBlock>
        ))
      ) : (
        <AligmentBlock>No selected goal</AligmentBlock>
      )}
    </AligmentSection>
  );
};

interface GoalSectionProps {
  sdgGoals: string[];
}

const AligmentSection = styled.div`
  margin-bottom: 32px;
`;

const AligmentBlock = styled.div`
  width: 112px;
  height: 112px;
  border-radius: 6px;
  background-color: ${color.line};
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;
`;
