import React, { HTMLAttributes } from 'react';
import styled from 'styled-components';

import { Title } from './Title';
import { color } from 'src/styles/color';

export const SectionTitle: React.FC<SectionTitleProps> = ({ title, link }) => {
  return (
    <Root>
      <Title type="h3" tag="h3">
        {title}
      </Title>
      {link && <Link href={link}>see more</Link>}
    </Root>
  );
};

interface SectionTitleProps extends HTMLAttributes<HTMLDivElement> {
  title: string;
  link?: string;
}

const Root = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 16px;
`;

const Link = styled.a`
  font-size: 12px;
  line-height: 150%;
  color: ${color.blue};
`;
