import React, { ButtonHTMLAttributes } from 'react';
import styled, { CSSProp } from 'styled-components';

export const AppButton = ({
  children,
  containerStyles = {},
  ...props
}: AppButtonProps): JSX.Element => {
  return (
    <Root $CSS={containerStyles} {...props}>
      {children}
    </Root>
  );
};

interface AppButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  containerStyles?: CSSProp;
}

type RootType = {
  $CSS?: CSSProp;
};

const Root = styled.button<RootType>`
  padding: 0;
  position: absolute;
  top: 50%;
  right: 16px;
  transform: translateY(-50%);
  width: 24px;
  height: 24px;
  background-color: transparent;
  border: none;
  cursor: pointer;
  ${({ $CSS }) => $CSS}
`;
