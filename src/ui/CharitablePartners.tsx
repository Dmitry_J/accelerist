import React from 'react';
import { color } from 'src/styles/color';
import styled from 'styled-components';

export const CharitablePartners: React.FC<CharitablePartnersProps> = ({
  charitablePartners,
}) => {
  return (
    <CharitableContainer>
      <TitleBlock>Charitable partners</TitleBlock>
      <CharitableBlock>
        {charitablePartners.length > 0 ? (
          <ul>
            {charitablePartners.map((item: string) => (
              <li key={item}>{item}</li>
            ))}
          </ul>
        ) : (
          'No charitable partners'
        )}
      </CharitableBlock>
    </CharitableContainer>
  );
};

interface CharitablePartnersProps {
  charitablePartners: string[];
}

const TitleBlock = styled.h3`
  font-weight: 500;
  font-size: 16px;
  line-height: 145%;
  margin-bottom: 16px;
`;

const CharitableContainer = styled.div`
  margin-bottom: 32px;
`;

const CharitableBlock = styled.div`
  border: 1px solid ${color.line};
  border-radius: 6px;
  padding: 24px;
`;
