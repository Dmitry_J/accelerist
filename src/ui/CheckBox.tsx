import React, { HTMLAttributes } from 'react';
import styled, { CSSProp } from 'styled-components';

import checkOn from 'src/assets/images/checkOn.svg';
import checkOff from 'src/assets/images/checkOff.svg';
import { color } from 'src/styles/color';

export const CheckBox = ({
  isCheked,
  containerStyles = {},
  labelStyles = {},
  title,
  ...props
}: CheckBoxProps): JSX.Element => {
  return (
    <Container $CSS={containerStyles}>
      <InputCheckbox type="checkbox" {...props} id={title} />
      <LabelRoot $CSS={labelStyles} htmlFor={title} check={isCheked}>
        {title}
      </LabelRoot>
    </Container>
  );
};

interface CheckBoxProps extends HTMLAttributes<HTMLDivElement> {
  isCheked: boolean;
  title: string;
  containerStyles?: CSSProp;
  labelStyles?: CSSProp;
}

type LabelType = {
  check?: boolean;
  $CSS?: CSSProp;
};

type ContainerType = {
  $CSS?: CSSProp;
};

const InputCheckbox = styled.input`
  display: none;
`;

const Container = styled.div<ContainerType>`
  ${({ $CSS }) => $CSS}
`;

const LabelRoot = styled.label<LabelType>`
  position: relative;
  display: inline-block;
  font-family: 'Rubik';
  font-weight: normal;
  font-size: 12px;
  line-height: 150%;
  padding-left: 35px;
  color: ${color.black};
  cursor: pointer;
  &::before {
    position: absolute;
    content: '';
    left: 0;
    top: 50%;
    transform: translateY(-50%);
    width: 24px;
    height: 24px;
    background-image: url(${({ check }) => (check ? checkOn : checkOff)});
  }
  ${({ $CSS }) => $CSS}
`;
