import React from 'react';
import styled from 'styled-components';

import { color } from 'src/styles/color';
import Logo from 'src/assets/images/icons/Logo';

export const HeaderEnter = (): JSX.Element => {
  return (
    <Header>
      <Container>
        <Logo />
        <Title>Accelerist</Title>
      </Container>
    </Header>
  );
};

const Header = styled.header`
  height: 80px;
  background-color: ${color.black};
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
`;

const Title = styled.span`
  margin-left: 16px;
  font-family: 'Gotham Rounded';
  font-style: normal;
  font-weight: 500;
  font-size: 18px;
  line-height: 145%;
  letter-spacing: 0.2em;
  color: ${color.white};
  text-transform: uppercase;
`;
