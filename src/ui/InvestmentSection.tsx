import React from 'react';
import { color } from 'src/styles/color';
import styled from 'styled-components';
import { Title } from './Title';

export const InvestmentSection: React.FC<InvestmentSectionProps> = () => {
  return (
    <InvestContainer>
      <InvestWrapper>
        <Title type="h4" tag="h4">
          Type of Investment
        </Title>
        <InvestBlock>
          <ul>
            <InvestItem>No information</InvestItem>
          </ul>
        </InvestBlock>
      </InvestWrapper>
      <InvestWrapper>
        <Title type="h4" tag="h4">
          CRS Focus
        </Title>
        <InvestBlock>
          <ul>
            <InvestItem>No information</InvestItem>
          </ul>
        </InvestBlock>
      </InvestWrapper>
    </InvestContainer>
  );
};

interface InvestmentSectionProps {}

const InvestContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 32px;
`;

const InvestWrapper = styled.div`
  display: flex;
  flex-direction: column;
  flex-basis: 48%;
`;

const InvestBlock = styled.div`
  border: 1px solid ${color.line};
  border-radius: 6px;
  padding: 24px;
  flex-grow: 1;
`;

const InvestItem = styled.li`
  position: relative;
  font-size: 16px;
  line-height: 155%;
  margin-bottom: 12px;
  padding-left: 14px;
  &:last-child {
    margin-bottom: 0;
  }
  &::before {
    content: '';
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    width: 6px;
    height: 6px;
    background-color: ${color.blue};
    border-radius: 50%;
  }
`;
