import React from 'react';
import styled from 'styled-components';
import Slider, { SliderProps } from '@mui/material/Slider';
import { color } from 'src/styles/color';

export const AppInputRange: React.FC<AppInputRangeProps> = ({
  value,
  ...props
}) => {
  return <Root value={value} {...props} />;
};

interface AppInputRangeProps extends SliderProps {
  value: number[];
}

const Root = styled(Slider)`
  span.MuiSlider-rail {
    background-color: ${color.line};
    height: 2px;
  }

  span.MuiSlider-track {
    border: none;
    background-color: ${color.blue};
    height: 2px;
  }

  span.MuiSlider-valueLabelOpen {
    background-color: ${color.white};
    border: 1px solid ${color.secondaryBlue};
    border-radius: 6px;
    color: ${color.black};
    width: 68px;
    height: 32px;
    transform: none;
    top: -6px;
    &::before {
      content: none;
    }
  }

  span.MuiSlider-thumbSizeMedium {
    box-shadow: none;
  }

  /* span[data-index='0'] {
    & span.MuiSlider-valueLabelOpen {
      left: 0;
    }
  }

  span[data-index='1'] {
    & span.MuiSlider-valueLabelOpen {
      right: 0;
    }
  } */
`;
