import React, { ButtonHTMLAttributes } from 'react';
import styled, { CSSProp } from 'styled-components';
import ReactLoading from 'react-loading';

import { color } from 'src/styles/color';
import { RequestTypes } from 'src/types/types';

export const Button = ({
  text,
  containerStyles = {},
  loading,
  spinColor = color.blue,
  ...props
}: ButtonProps): JSX.Element => {
  return (
    <Root $CSS={containerStyles} {...props}>
      {loading === RequestTypes.PENDING ? (
        <ReactLoading type="spin" color={spinColor} height={20} width={20} />
      ) : (
        text
      )}
    </Root>
  );
};

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  text: string;
  containerStyles?: CSSProp;
  loading?: string;
  spinColor?: string;
}

type ButtonRoot = {
  $CSS?: CSSProp;
};

const Root = styled.button<ButtonRoot>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  border: none;
  height: 40px;
  color: ${color.white};
  background-color: ${color.blue};
  border-radius: 6px;
  transition: 0.25s;
  cursor: pointer;
  &:disabled {
    background-color: ${color.secondaryBlue};
    color: ${color.blue};
    cursor: not-allowed;
  }
  ${({ $CSS }) => $CSS};
`;
