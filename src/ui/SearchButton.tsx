import React, { ButtonHTMLAttributes } from 'react';
import styled from 'styled-components';

import SearchIcon from 'src/assets/images/icons/SearchIcon';

export const SearchButton = ({
  ...props
}: ButtonHTMLAttributes<HTMLButtonElement>): JSX.Element => {
  return (
    <Root {...props}>
      <SearchIcon />
    </Root>
  );
};

const Root = styled.button`
  padding: 0;
  position: absolute;
  top: 50%;
  right: 16px;
  transform: translateY(-50%);
  width: 24px;
  height: 24px;
  background-color: transparent;
  border: none;
  cursor: pointer;
`;
