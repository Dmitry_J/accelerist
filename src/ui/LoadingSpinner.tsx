import React from 'react';
import styled from 'styled-components';
import ReactLoading from 'react-loading';

import { color } from 'src/styles/color';

export const LoadingSpinner = (): JSX.Element => {
  return (
    <Root>
      <ReactLoading type="spin" color={color.blue} height={80} width={80} />
    </Root>
  );
};

const Root = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0;
  height: calc(100% - 180px);
`;
