import React from 'react';
import styled, { CSSProp } from 'styled-components';
import { Link, LinkProps } from 'react-router-dom';
import { color } from 'src/styles/color';

export const AppLink: React.FC<AppLinkProps> = ({
  children,
  containerStyles = {},
  ...props
}) => {
  return (
    <Root {...props} $CSS={containerStyles}>
      {children}
    </Root>
  );
};

interface AppLinkProps extends LinkProps {
  containerStyles?: CSSProp;
}

type RootType = {
  $CSS?: CSSProp;
};

const Root = styled(Link)<RootType>`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 40px;
  border-radius: 6px;
  transition: 0.25s;
  flex-grow: 1;
  border: 1px solid ${color.blue};
  border-radius: 6px;
  height: 36px;
  background-color: ${color.white};
`;
